# GG dddd changed
Page semi-protected Listen to this article
Human history
From Wikipedia, the free encyclopedia
Jump to navigationJump to search
This article is about the history of humanity. For the entire history of Earth, see History of Earth. For the field of historical study, see World history. For humanity's evolutionary history, see Human evolution.
Part of a series on
Human history
Human Era
↑ Prehistory   (Pleistocene epoch)
Holocene
Timelines
Neolithic – Contemporary
(10,000 BCE – 2020 CE)
Age of the human raceRecorded history
Earliest recordsProtohistoryProto-writing
Ancient
Bronze ageIron age
Axial antiquityClassical antiquityLate antiquity
AfricaNorth AmericaSouth America
OceaniaEast AsiaSouth Asia
Southeast AsiaWest Asia
Europe
Postclassical
AfricaAmericas
OceaniaEast Asia
South Asia
Southeast AsiaWest Asia
Europe
Modern
Early modernLate modern
AfricaNorth AmericaSouth America
OceaniaEast AsiaSouth Asia
Southeast AsiaWest Asia
Europe
See also
ModernityFuturology
↓ Future   
vte

World population, 10,000 BCE – 2,000 CE (vertical population scale is logarithmic)[1]
Human history (or the history of humanity), also known as the history of the world, is the carefully researched description of humanity's past. It is informed by archaeology, anthropology, genetics, linguistics, and other disciplines; and, for periods since the invention of writing, by recorded history and by secondary sources and studies.

Humanity's written history was preceded by its prehistory, beginning with the Palaeolithic Era ("Old Stone Age"), followed by the Neolithic Era ("New Stone Age"). The Neolithic saw the Agricultural Revolution begin, between 10,000 and 5000 BCE, in the Near East's Fertile Crescent. During this period, humans began the systematic husbandry of plants and animals.[2] As agriculture advanced, most humans transitioned from a nomadic to a settled lifestyle as farmers in permanent settlements. The relative security and increased productivity provided by farming allowed communities to expand into increasingly larger units, fostered by advances in transportation.

Whether in prehistoric or historic times, people always needed to be near reliable sources of potable water. Settlements developed as early as 4,000 BCE in Iran,[3][4][5][6][7] in Mesopotamia,[8] in the Indus River valley,[9] on the banks of Egypt's Nile River,[10][11] and along China's rivers.[12][13] As farming developed, grain agriculture became more sophisticated and prompted a division of labour to store food between growing seasons. Labour divisions led to the rise of a leisured upper class and the development of cities, which provided the foundation for civilization. The growing complexity of human societies necessitated systems of accounting and writing.

With civilizations flourishing, ancient history ("Antiquity," including the Classical Age,[14] up to about 500 CE[15]) saw the rise and fall of empires. Post-classical history (the "Middle Ages," c. 500–1500 CE,[16]) witnessed the rise of Christianity, the Islamic Golden Age (c. 750 CE – c. 1258 CE), the Timurid and Italian Renaissance (from around 1300 CE). The mid-15th-century introduction of movable-type printing in Europe[17] revolutionized communication and facilitated ever wider dissemination of information, hastening the end of the Middle Ages and ushering in the Scientific Revolution.[18] The Early Modern Period, sometimes referred to as the "European Age and Age of the Islamic Gunpowders",[19] from about 1500 to 1800,[20] included the Age of Enlightenment and the Age of Exploration. By the 18th century, the accumulation of knowledge and technology had reached a critical mass that brought about the Industrial Revolution[21] and began the Late Modern Period, which started around 1800 and has continued through the present.[16]

This scheme of historical periodization (dividing history into Antiquity, Post-Classical, Early Modern, and Late Modern periods) was developed for, and applies best to, the history of the Old World, particularly Europe and the Mediterranean. Outside this region, including ancient China and ancient India, historical timelines unfolded differently. However, by the 18th century, due to extensive world trade and colonization, the histories of most civilizations had become substantially intertwined, a process known as globalization. In the last quarter-millennium, the rates of growth of population, knowledge, technology, communications, commerce, weapons destructiveness, and environmental degradation have greatly accelerated, creating opportunities and perils that now confront the planet's human communities.[22]


Contents
1	Prehistory
1.1	Early humans
1.2	Rise of civilization
2	Ancient history
2.1	Cradles of civilization
2.2	Axial Age
2.3	Regional empires
2.4	Declines, falls, and resurgence
3	Post-classical history
3.1	Greater Middle East
3.2	Europe
3.3	Sub-Saharan Africa
3.4	South Asia
3.5	Northeast Asia
3.6	Southeast Asia
3.7	Oceania
3.8	Americas
4	Modern history
4.1	Early modern period
4.2	Late modern period
4.3	Contemporary history
5	See also
6	Explanatory notes
7	References
8	Bibliography
9	Further reading
Prehistory
Main articles: Prehistory, Human evolution, and Timeline of human prehistory
Early humans
Genetic measurements indicate that the ape lineage which would lead to Homo sapiens diverged from the lineage that would lead to chimpanzees and bonobos, the closest living relatives of modern humans, around 4.6 to 6.2 million years ago.[23] Anatomically modern humans arose in Africa about 300,000 years ago,[24] and reached behavioural modernity about 50,000 years ago.[25]


Cave painting, Lascaux, France, c. 15,000 BCE

"Venus of Willensdorf", Austria, c. 26,500 BCE
Modern humans spread rapidly from Africa into the frost-free zones of Europe and Asia around 60,000 years ago.[26] The rapid expansion of humankind to North America and Oceania took place at the climax of the most recent ice age, when temperate regions of today were extremely inhospitable. Yet, humans had colonized nearly all the ice-free parts of the globe by the end of the Ice Age, some 12,000 years ago.[27] Other hominids such as Homo erectus had been using simple wood and stone tools for millennia, but as time progressed, tools became far more refined and complex.

Perhaps as early as 1.8 million years ago, but certainly by 500,000 years ago, humans began using fire for heat and cooking.[28] They also developed language in the Paleolithic period[29] and a conceptual repertoire that included systematic burial of the dead and adornment of the living. Early artistic expression can be found in the form of cave paintings and sculptures made from ivory, stone, and bone, showing a spirituality generally interpreted as animism, or even shamanism.[30] During this period, all humans lived as hunter-gatherers, and were generally nomadic.[31] Archaeological and genetic data suggest that the source populations of Paleolithic hunter-gatherers survived in sparsely wooded areas and dispersed through areas of high primary productivity while avoiding dense forest cover.[32]

Rise of civilization
The Neolithic Revolution, beginning around 10,000 BCE, saw the development of agriculture, which fundamentally changed the human lifestyle. Farming developed around 10,000 BCE in the Middle East, around 7000 BCE in what is now China, around 6000 BCE in the Indus Valley and Europe, and around 4000 BCE in the Americas.[33] Cultivation of cereal crops and the domestication of animals occurred around 8500 BCE in the Middle East, where wheat and barley were the first crops and sheep and goats were domesticated.[34] In the Indus Valley, crops were cultivated by 6000 BCE, along with domesticated cattle. The Yellow River valley in China cultivated millet and other cereal crops by about 7000 BCE, but the Yangtze valley domesticated rice earlier, by at least 8000 BCE. In the Americas, sunflowers were cultivated by about 4000 BCE, and maize and beans were domesticated in Central America by 3500 BCE. Potatoes were first cultivated in the Andes Mountains of South America, where the llama was also domesticated.[33] Metal-working, starting with copper around 6000 BCE, was first used for tools and ornaments. Gold soon followed, with its main use being for ornaments. The need for metal ores stimulated trade, as many of the areas of early human settlement were lacking in ores. Bronze, an alloy of copper and tin, was first known from around 2500 BCE, but did not become widely used until much later.[35]


Monumental cuneiform inscription, Sumer, Mesopotamia, 26th century BCE
Though early proto-cities appeared at Jericho and Catal Huyuk around 6000 BCE,[36] the first civilizations did not emerge until around 3000 BCE in Egypt[37] and Mesopotamia.[38] These cultures gave birth to the invention of the wheel,[39] mathematics,[40] bronze-working, sailing boats, the pottery wheel, woven cloth, construction of monumental buildings,[41] and writing.[42] Scholars now recognize that writing may have independently developed in at least four ancient civilizations: Mesopotamia (between 3400 and 3100 BC), Egypt (around 3250 BC),[43][44] China (2000 BC),[45] and lowland Mesoamerica (by 650 BC).[46]

Farming permitted far denser populations, which in time organized into states. Agriculture also created food surpluses that could support people not directly engaged in food production.[47] The development of agriculture permitted the creation of the first cities. These were centres of trade, manufacturing and political power.[48] Cities established a symbiosis with their surrounding countrysides, absorbing agricultural products and providing, in return, manufactured goods and varying degrees of military control and protection.


Pyramid text, pyramid of Unas, Saqqara, Egypt, 24th century BCE
The development of cities was synonymous with the rise of civilization.[a] Early civilizations arose first in Lower Mesopotamia (3000 BCE),[50][51] followed by Egyptian civilization along the Nile River (3000 BCE),[11] the Harappan civilization in the Indus River Valley (in present-day India and Pakistan; 2500 BCE),[52][53] and Chinese civilization along the Yellow and Yangtze Rivers (2200 BCE).[12][13] These societies developed a number of unifying characteristics, including a central government, a complex economy and social structure, sophisticated language and writing systems, and distinct cultures and religions. Writing facilitated the administration of cities, the expression of ideas, and the preservation of information.[54]

Entities such as the Sun, Moon, Earth, sky, and sea were often deified.[55] Shrines developed, which evolved into temple establishments, complete with a complex hierarchy of priests and priestesses and other functionaries. Typical of the Neolithic was a tendency to worship anthropomorphic deities. Among the earliest surviving written religious scriptures are the Egyptian Pyramid Texts, the oldest of which date to between 2400 and 2300 BCE.[56]

Ancient history
Main articles: Ancient history and Timeline of ancient history
Ancient history
Preceded by prehistory
Near East
Sumer · Egypt · Elam · Akkad · Assyria · Babylonia · Mitanni · Hittites · Sea Peoples · Anatolia · Israel and Judah · Arabia · Berbers · Phoenicia · Persia
Europe
Minoans · Greece · Illyrians · Argaric · Nuragic · Tartessos · Iberia · Celts · Germanics · Etruscans · Rome · Slavs · Daco-Thracians
Eurasian Steppe
Proto-Indo-Europeans · Afanasievo · Indo-Iranians · Scythia · Tocharians · Huns · Xionites · Turks
East Asia
China · Japan · Korea · Mongolia
South Asia
Indus Valley Civilisation · Vedic period · Mahajanapadas · Nanda Empire · Maurya Empire · Sangam period · Middle Kingdoms · Gupta Empire
Mississippi and Oasisamerica
Adena · Hopewell · Mississippian · Puebloans
Mesoamerica
Olmecs · Epi-Olmec · Zapotec · Mixtec · Maya · Teotihuacan · Toltec Empire
Andes
Norte Chico · Sechin · Chavín · Paracas · Nazca · Moche · Lima · Tiwanaku · Wari
West Africa
Dhar Tichitt · Oualata · Nok · Senegambia · Djenné-Djenno · Bantu · Ghana Empire
Southeast Asia and Oceania
Vietnam · Austronesians · Australia · Polynesia · Funan · Tarumanagara
See also
History of the world · Ancient maritime history
Protohistory · Axial Age · Iron Age
Historiography · Ancient literature
Ancient warfare · Cradle of civilization

Category Category
Followed by Post-classical history
vte
Cradles of civilization
Main articles: Cradle of civilization, Bronze Age, and Iron Age

Great Pyramids of Giza, Egypt
The Bronze Age is part of the three-age system (Stone Age, Bronze Age, Iron Age) that for some parts of the world describes effectively the early history of civilization. During this era the most fertile areas of the world saw city-states and the first civilizations develop. These were concentrated in fertile river valleys: the Tigris and Euphrates in Mesopotamia, the Nile in Egypt,[57] the Indus in the Indian subcontinent,[52] and the Yangtze and Yellow Rivers in China.

Sumer, located in Mesopotamia, is the first known complex civilization, developing the first city-states in the 4th millennium BCE.[51] It was in these cities that the earliest known form of writing, cuneiform script, appeared around 3000 BCE.[58][59] Cuneiform writing began as a system of pictographs. These pictorial representations eventually became simplified and more abstract.[59] Cuneiform texts were written on clay tablets, on which symbols were drawn with a blunt reed used as a stylus.[58] Writing made the administration of a large state far easier.

Transport was facilitated by waterways—by rivers and seas. The Mediterranean Sea, at the juncture of three continents, fostered the projection of military power and the exchange of goods, ideas, and inventions. This era also saw new land technologies, such as horse-based cavalry and chariots, that allowed armies to move faster.


Fresco, Knossos, Minoan Crete
These developments led to the rise of territorial states and empires. In Mesopotamia there prevailed a pattern of independent warring city-states and of a loose hegemony shifting from one city to another.[citation needed] In Egypt, by contrast, first there was a dual division into Upper and Lower Egypt which was shortly followed by unification of all the valley around 3100 BCE, followed by permanent pacification.[60] In Crete the Minoan civilization had entered the Bronze Age by 2700 BCE and is regarded as the first civilization in Europe.[61] Over the next millennia, other river valleys saw monarchical empires rise to power.[citation needed] In the 25th – 21st centuries BCE, the empires of Akkad and Sumer arose in Mesopotamia.[62]

Over the following millennia, civilizations developed across the world. Trade increasingly became a source of power as states with access to important resources or controlling important trade routes rose to dominance.[citation needed] By 1400 BCE, Mycenaean Greece began to develop.[63] In India this era was the Vedic period, which laid the foundations of Hinduism and other cultural aspects of early Indian society, and ended in the 6th century BCE.[64] From around 550 BCE, many independent kingdoms and republics known as the Mahajanapadas were established across the subcontinent.[65]

As complex civilizations arose in the Eastern Hemisphere, the indigenous societies in the Americas remained relatively simple and fragmented into diverse regional cultures. During the formative stage in Mesoamerica (about 1500 BCE to 500 CE), more complex and centralized civilizations began to develop, mostly in what is now Mexico, Central America, and Peru. They included civilizations such as the Olmec, Maya, Zapotec, Moche, and Nazca. They developed agriculture, growing maize, chili peppers, cocoa, tomatoes, and potatoes, crops unique to the Americas, and creating distinct cultures and religions. These ancient indigenous societies would be greatly affected, for good and ill, by European contact during the early modern period.

Axial Age
Main articles: Axial Age, History of philosophy, Timeline of religion, and History of religions

The Buddha

Socrates
Beginning in the 8th century BCE, the "Axial Age" saw the development of a set of transformative philosophical and religious ideas, mostly independently, in many different places.[citation needed] Chinese Confucianism, Indian Buddhism and Jainism, and Jewish monotheism are all claimed by some scholars to have developed in the 6th century BCE. (Karl Jaspers' Axial-Age theory also includes Persian Zoroastrianism, but other scholars dispute his timeline for Zoroastrianism.) In the 5th century BCE, Socrates and Plato made substantial advances in the development of ancient Greek philosophy.

In the East, three schools of thought would dominate Chinese thinking well into the 20th century. These were Taoism, Legalism, and Confucianism. The Confucian tradition, which would become particularly dominant, looked for political morality not to the force of law but to the power and example of tradition. Confucianism would later spread to the Korean Peninsula and toward Japan.

In the West, the Greek philosophical tradition, represented by Socrates, Plato, Aristotle, and other philosophers,[66] along with accumulated science, technology, and culture, diffused throughout Europe, Egypt, the Middle East, and Northwest India, starting in the 4th century BCE after the conquests of Alexander III of Macedon (Alexander the Great).[67]

Regional empires
Main articles: Civilization and Empire
The millennium from 500 BCE to 500 CE saw a series of empires of unprecedented size develop. Well-trained professional armies, unifying ideologies, and advanced bureaucracies created the possibility for emperors to rule over large domains whose populations could attain numbers upwards of tens of millions of subjects. The great empires depended on military annexation of territory and on the formation of defended settlements to become agricultural centres. The relative peace that the empires brought encouraged international trade, most notably the massive trade routes in the Mediterranean, the maritime trade web in the Indian Ocean, and the Silk Road. In southern Europe, the Greeks (and later the Romans), in an era known as "classical antiquity," established cultures whose practices, laws, and customs are considered the foundation of contemporary Western culture.


Persepolis, Achaemenid Empire, 6th century BCE

Parthenon, Athenian Empire

Pillar erected by India's Maurya Dynasty Emperor Ashoka

Trajan's Column, Rome

Terracotta army, China, c. 210 BCE

Obelisk of Aksum, Ethiopia
There were a number of regional empires during this period. The kingdom of the Medes helped to destroy the Assyrian Empire in tandem with the nomadic Scythians and the Babylonians. Nineveh, the capital of Assyria, was sacked by the Medes in 612 BCE.[68] The Median Empire gave way to successive Iranian empires, including the Achaemenid Empire (550–330 BCE), the Parthian Empire (247 BCE–224 CE), and the Sasanian Empire (224–651 CE).

Several empires began in modern-day Greece. First was the Delian League (from 477 BCE)[69] and the succeeding Athenian Empire (454–404 BCE), centred in present-day Greece. Later, Alexander the Great (356–323 BCE), of Macedon, founded an empire of conquest, extending from present-day Greece to present-day India.[70][71] The empire divided shortly after his death, but the influence of his Hellenistic successors made for an extended Hellenistic period (323–31 BCE)[72] throughout the region.

In Asia, the Maurya Empire (322–185 BCE) existed in present-day India;[73] in the 3rd century BCE, most of South Asia was united to the Maurya Empire by Chandragupta Maurya and flourished under Ashoka the Great. From the 3rd century CE, the Gupta dynasty oversaw the period referred to as ancient India's Golden Age. From the 4th to 6th centuries, northern India was ruled by the Gupta Empire. In southern India, three prominent Dravidian kingdoms emerged: the Cheras,[citation needed] Cholas,[74] and Pandyas. The ensuing stability contributed to heralding in the golden age of Hindu culture in the 4th and 5th centuries.

In Europe, the Roman Empire, centered in present-day Italy, began in the 7th century BCE.[75] In the 3rd century BCE the Roman Republic began expanding its territory through conquest and alliances.[76] By the time of Augustus (63 BCE – 14 CE), the first Roman Emperor, Rome had already established dominion over most of the Mediterranean. The empire would continue to grow, controlling much of the land from England to Mesopotamia, reaching its greatest extent under the emperor Trajan (died 117 CE). In the 3rd century CE, the empire split into western and eastern regions, with (usually) separate emperors. The Western empire would fall, in 476 CE, to German influence under Odoacer. The eastern empire, now known as the Byzantine Empire, with its capital at Constantinople, would continue for another thousand years, until Constantinople was conquered by the Ottoman Empire in 1453.

In China, the Qin dynasty (221–206 BCE), the first imperial dynasty of China, was followed by the Han Empire (206 BCE – 220 CE). The Han Dynasty was comparable in power and influence to the Roman Empire that lay at the other end of the Silk Road. Han China developed advanced cartography, shipbuilding, and navigation. The Chinese invented blast furnaces, and created finely tuned copper instruments. As with other empires during the Classical Period, Han China advanced significantly in the areas of government, education, mathematics, astronomy, technology, and many others.[77]


Maya observatory, Chichen Itza, Mexico
In Africa, the Kingdom of Aksum, centred in present-day Ethiopia, established itself by the 1st century CE as a major trading empire, dominating its neighbours in South Arabia and Kush and controlling the Red Sea trade. It minted its own currency and carved enormous monolithic steles such as the Obelisk of Axum to mark their emperors' graves.

Successful regional empires were also established in the Americas, arising from cultures established as early as 2500 BCE.[78] In Mesoamerica, vast pre-Columbian societies were built, the most notable being the Zapotec Empire (700 BCE – 1521 CE),[79] and the Maya civilization, which reached its highest state of development during the Mesoamerican Classic period (c. 250–900 CE),[80] but continued throughout the Post-Classic period until the arrival of the Spanish in the 16th century CE. Maya civilization arose as the Olmec mother culture gradually declined. The great Mayan city-states slowly rose in number and prominence, and Maya culture spread throughout the Yucatán and surrounding areas. The later empire of the Aztecs was built on neighbouring cultures and was influenced by conquered peoples such as the Toltecs.

Some areas experienced slow but steady technological advances, with important developments such as the stirrup and moldboard plough arriving every few centuries. There were, however, in some regions, periods of rapid technological progress. Most important, perhaps, was the Hellenistic period in the region of the Mediterranean, during which hundreds of technologies were invented.[81] Such periods were followed by periods of technological decay, as during the Roman Empire's decline and fall and the ensuing early medieval period.

Declines, falls, and resurgence
The ancient empires faced common problems associated with maintaining huge armies and supporting a central bureaucracy. These costs fell most heavily on the peasantry, while land-owning magnates increasingly evaded centralized control and its costs. Barbarian pressure on the frontiers hastened internal dissolution. China's Han dynasty fell into civil war in 220 CE, beginning the Three Kingdoms period, while its Roman counterpart became increasingly decentralized and divided about the same time in what is known as the Crisis of the Third Century. The great empires of Eurasia were all located on temperate and subtropical coastal plains. From the Central Asian steppes, horse-based nomads, mainly Mongols and Turks, dominated a large part of the continent. The development of the stirrup and the breeding of horses strong enough to carry a fully armed archer made the nomads a constant threat to the more settled civilizations.


The Pantheon in Rome, Italy, originally a Roman temple, now a Catholic church
The gradual break-up of the Roman Empire, spanning several centuries after the 2nd century CE, coincided with the spread of Christianity outward from the Middle East.[82] The Western Roman Empire fell under the domination of Germanic tribes in the 5th century,[83] and these polities gradually developed into a number of warring states, all associated in one way or another with the Catholic Church.[84] The remaining part of the Roman Empire, in the eastern Mediterranean, continued as what came to be called the Byzantine Empire.[85] Centuries later, a limited unity would be restored to western Europe through the establishment in 962 of a revived "Roman Empire",[86] later called the Holy Roman Empire,[87] comprising a number of states in what is now Germany, Austria, Switzerland, Czech Republic, Belgium, Italy, and parts of France.[88][89]

In China, dynasties would rise and fall, but, by sharp contrast to the Mediterranean-European world, dynastic unity would be restored. After the fall of the Eastern Han Dynasty[90] and the demise of the Three Kingdoms, nomadic tribes from the north began to invade in the 4th century, eventually conquering areas of northern China and setting up many small kingdoms.[citation needed] The Sui Dynasty successfully reunified the whole of China[91] in 581,[92] and laid the foundations for a Chinese golden age under the Tang dynasty (618–907).

Post-classical history
Main article: Post-classical history

University of Timbuktu, Mali
The term "Post-classical Era", though derived from the Eurocentric name of the era of "Classical antiquity", takes in a broader geographic sweep. The era is commonly dated from the 5th-century fall of the Western Roman Empire, which fragmented into many separate kingdoms, some of which would later be confederated under the Holy Roman Empire.

The Eastern Roman, or Byzantine, Empire survived until late in the Post-classical, or Medieval, period.

The Post-classical period also encompasses the Early Muslim conquests, the subsequent Islamic Golden Age, and the commencement and expansion of the Arab slave trade, followed by the Mongol invasions of the Middle East, Central Asia, and Eastern Europe[citation needed] and the founding around 1280 of the Ottoman Empire.[93] South Asia saw a series of middle kingdoms of India, followed by the establishment of Islamic empires in India.

In western Africa, the Mali Empire and the Songhai Empire developed. On the southeast coast of Africa, Arabic ports were established where gold, spices, and other commodities were traded. This allowed Africa to join the Southeast Asia trading system, bringing it contact with Asia; this, along with Muslim culture, resulted in the Swahili culture.

China experienced the successive Sui, Tang, Song, Yuan, and early Ming dynasties. Middle Eastern trade routes along the Indian Ocean, and the Silk Road through the Gobi Desert, provided limited economic and cultural contact between Asian and European civilizations.

During the same period, civilizations in the Americas, such as the Inca, Maya, and Aztecs, reached their zenith. All would be compromised by, then conquered after, contact with European colonists at the beginning of the Modern period.

Greater Middle East
Main articles: History of the Middle East, History of North Africa, History of Central Asia, History of the Caucasus, and Islamic Golden Age
Prior to the advent of Islam in the 7th century, the Middle East was dominated by the Byzantine Empire and the Persian Sasanian Empire, which frequently fought each other for control of several disputed regions. This was also a cultural battle, with the Byzantine Hellenistic and Christian culture competing against the Persian Iranian traditions and Zoroastrian religion. The formation of the Islamic religion created a new contender that quickly surpassed both of these empires. Islam greatly affected the political, economic, and military history of the Old World, especially the Middle East.


Great Mosque of Kairouan, Tunisia, founded 670 CE
From their centre on the Arabian Peninsula, Muslims began their expansion during the early Postclassical Era. By 750 CE, they came to conquer most of the Near East, North Africa, and parts of Europe, ushering in an era of learning, science, and invention known as the Islamic Golden Age. The knowledge and skills of the ancient Near East, Greece, and Persia were preserved in the Postclassical Era by Muslims, who also added new and important innovations from outside, such as the manufacture of paper from China and decimal positional numbering from India.

Much of this learning and development can be linked to geography. Even prior to Islam's presence, the city of Mecca had served as a centre of trade in Arabia, and the Islamic prophet Muhammad himself was a merchant. With the new Islamic tradition of the Hajj, the pilgrimage to Mecca, the city became even more a centre for exchanging goods and ideas. The influence held by Muslim merchants over African-Arabian and Arabian-Asian trade routes was tremendous. As a result, Islamic civilization grew and expanded on the basis of its merchant economy, in contrast to the Europeans, Indians, and Chinese, who based their societies on an agricultural landholding nobility. Merchants brought goods and their Islamic faith to China, India, Southeast Asia, and the kingdoms of western Africa, and returned with new discoveries and inventions.


Crusader Krak des Chevaliers, Syria
Motivated by religion and dreams of conquest, European leaders launched a number of Crusades to try to roll back Muslim power and retake the Holy Land. The Crusades were ultimately unsuccessful and served more to weaken the Byzantine Empire, especially with the 1204 sack of Constantinople. The Byzantine Empire began to lose increasing amounts of territory to the Ottoman Turks. Arab domination of the region ended in the mid-11th century with the arrival of the Seljuq Turks, migrating south from the Turkic homelands in Central Asia. In the early 13th century, a new wave of invaders, the Mongol Empire, swept through the region but were eventually eclipsed by the Turks[citation needed] and the founding of the Ottoman Empire in modern-day Turkey around 1280.[93]

North Africa saw the rise of polities formed by the Berbers, such as the Marinid dynasty in Morocco, the Zayyanid dynasty in Algeria, and the Hafsid dynasty in Tunisia. The region will later be called the Barbary Coast and will host pirates and privateers who will use several North African ports for their raids against the coastal towns of several European countries in search of slaves to be sold in North African markets as part of the Barbary slave trade.

Starting with the Sui dynasty (581–618), the Chinese began expanding into eastern Central Asia, and confronted Turkic nomads, who were becoming the most dominant ethnic group in Central Asia.[94][95] Originally the relationship was largely cooperative, but in 630 the Tang dynasty began an offensive against the Turks,[96] capturing areas of the Mongolian Ordos Desert. In the 8th century, Islam began to penetrate the region and soon became the sole faith of most of the population, though Buddhism remained strong in the east.[weasel words][citation needed] The desert nomads of Arabia could militarily match the nomads of the steppe, and the early Arab Empire gained control over parts of Central Asia.[94] The Hephthalites were the most powerful of the nomad groups in the 6th and 7th centuries, and controlled much of the region. In the 9th through 13th centuries the region was divided among several powerful states, including the Samanid Empire[citation needed] the Seljuk Empire,[97] and the Khwarezmid Empire. The largest empire to rise out of Central Asia developed when Genghis Khan united the tribes of Mongolia. The Mongol Empire spread to comprise all of Central Asia and China as well as large parts of Russia and the Middle East.[citation needed] After Genghis Khan died in 1227,[98] most of Central Asia continued to be dominated by a successor state, Chagatai Khanate. In 1369, Timur, a Turkic leader in the Mongol military tradition, conquered most of the region and founded the Timurid Empire. Timur's large empire collapsed soon after his death, however. The region then became divided into a series of smaller khanates that were created by the Uzbeks. These included the Khanate of Khiva, the Khanate of Bukhara, and the Khanate of Kokand, all of whose capitals are located in present-day Uzbekistan.

In the aftermath of the Byzantine–Sasanian wars, the Caucasus saw Armenia and Georgia flourish as independent realms free from foreign suzerainty. However, with the Byzantine and Sasanian empires exhausted from war, the Arabs were given the opportunity to proceed to the Caucasus during the early Muslim conquests. By the 13th century, the arrival of the Mongols saw the region invaded and subjugated once again.

Europe
Main articles: History of Europe, Middle Ages, and Timeline of the Middle Ages
Europe during the Early Middle Ages was characterized by depopulation, deurbanization, and barbarian invasion, all of which had begun in Late Antiquity. The barbarian invaders formed their own new kingdoms in the remains of the Western Roman Empire. In the 7th century, North Africa and the Middle East, once part of the Eastern Roman Empire, became part of the Caliphate after conquest by Muhammad's successors. Although there were substantial changes in society and political structures, most of the new kingdoms incorporated as many of the existing Roman institutions as they could. Christianity expanded in western Europe, and monasteries were founded. In the 7th and 8th centuries the Franks, under the Carolingian dynasty, established an empire covering much of western Europe;[citation needed] it lasted until the 9th century, when it succumbed to pressure from new invaders—the Vikings,[99] Magyars, and Saracens.


St. Peter's Basilica, Vatican City
During the High Middle Ages, which began after 1000, the population of Europe increased greatly as technological and agricultural innovations allowed trade to flourish and crop yields to increase. Manorialism—the organization of peasants into villages that owed rents and labour service to nobles—and feudalism—a political structure whereby knights and lower-status nobles owed military service to their overlords in return for the right to rents from lands and manors—were two of the ways of organizing medieval society that developed during the High Middle Ages. Kingdoms became more centralized after the decentralizing effects of the break-up of the Carolingian Empire. The Crusades, first preached in 1095, were an attempt by western Christians from nations such as the Kingdom of England, the Kingdom of France and the Holy Roman Empire to regain control of the Holy Land from the Muslims and succeeded for long enough to establish some Christian states in the Near East. Italian merchants imported slaves to work in households or in sugar processing.[citation needed] Intellectual life was marked by scholasticism and the founding of universities, while the building of Gothic cathedrals was one of the outstanding artistic achievements of the age.

The Late Middle Ages were marked by difficulties and calamities. Famine, plague, and war devastated the population of western Europe.[citation needed] The Black Death alone killed approximately 75 to 200 million people between 1347 and 1350.[100][101] It was one of the deadliest pandemics in human history. Starting in Asia, the disease reached Mediterranean and western Europe during the late 1340s,[102] and killed tens of millions of Europeans in six years; between a third and a half of the population perished.

The Middle Ages witnessed the first sustained urbanization of northern and western Europe and it lasted until the beginning of the early modern period in the 16th century,[20] marked by the rise of nation states,[103] the division of Western Christianity in the Reformation,[104] the rise of humanism in the Italian Renaissance,[105] and the beginnings of European overseas expansion which allowed for the Columbian Exchange.

In Central and Eastern Europe, in 1386, the Kingdom of Poland and the Grand Duchy of Lithuania (the latter including territories of modern Belarus and Ukraine), facing depredations by the Teutonic Knights and later also threats from Muscovy, the Crimean Tatars, and the Ottoman Empire, formed a personal union through the marriage of Poland's Queen Jadwiga to Lithuanian Grand Duke Jogaila, who became King Władysław II Jagiełło of Poland. For the next four centuries, until the 18th-century Partitions of the Polish-Lithuanian Commonwealth by Prussia, Russia, and Austria, the two polities conducted a federated condominium, long Europe's largest state, which welcomed diverse ethnicities and religions, including most of the world's Jews, furthered scientific thought (e.g., Copernicus's heliocentric theory), and—in a last-ditch effort to preserve their sovereignty—adopted the Constitution of 3 May 1791, the world's second modern written constitution after the U.S. Constitution that went into effect in 1789.

Sub-Saharan Africa
Main article: History of Africa

Brass "Benin bronze", Nigeria
Medieval Sub-Saharan Africa was home to many different civilizations. The Kingdom of Aksum declined in the 7th century as Islam cut it off from its Christian allies and its people moved further into the Ethiopian Highlands for protection. They eventually gave way to the Zagwe dynasty who are famed for their rock cut architecture at Lalibela. The Zagwe would then fall to the Solomonic dynasty who claimed descent from the Aksumite emperors[citation needed] and would rule the country well into the 20th century. In the West African Sahel region, many Islamic empires rose, such as the Ghana Empire, the Mali Empire, the Songhai Empire, and the Kanem–Bornu Empire. They controlled the trans-Saharan trade in gold, ivory, salt and slaves.

South of the Sahel, civilizations rose in the coastal forests where horses and camels could not survive.[citation needed] These include the Yoruba city of Ife, noted for its art,[106] and the Oyo Empire, the Kingdom of Benin of the Edo people centred in Benin City, the Igbo Kingdom of Nri which produced advanced bronze art at Igbo-Ukwu, and the Akan who are noted for their intricate architecture.[citation needed]

Central Africa saw the birth of several states, including the Kingdom of Kongo. In what is now modern Southern Africa, native Africans created various kingdoms such as the Kingdom of Mutapa. They flourished through trade with the Swahili people on the East African coast. They built large defensive stone structures without mortar such as Great Zimbabwe, capital of the Kingdom of Zimbabwe, Khami, capital of Kingdom of Butua, and Danangombe (Dhlo-Dhlo), capital of the Rozwi Empire. The Swahili people themselves were the inhabitants of the East African coast from Kenya to Mozambique who traded extensively with Asians and Arabs, who introduced them to Islam. They built many port cities such as Mombasa, Zanzibar and Kilwa, which were known to Chinese sailors under Zheng He and Islamic geographers.

South Asia
Main article: History of India

Chennakesava Temple, Belur, India
In northern India, after the fall (550 CE) of the Gupta Empire, the region was divided into a complex and fluid network of smaller kingly states.[citation needed]

Early Muslim incursions began in the west in 712 CE, when the Arab Umayyad Caliphate annexed much of present-day Pakistan. Arab military advance was largely halted at that point, but Islam still spread in India, largely due to the influence of Arab merchants along the western coast.

The ninth century saw a Tripartite Struggle for control of northern India, among the Pratihara Empire, the Pala Empire, and the Rashtrakuta Empire. Some of the important states that emerged in India at this time included the Bahmani Sultanate and the Vijayanagara Empire.

Post-classical dynasties in South India included those of the Chalukyas, the Hoysalas, the Cholas, the Islamic Mughals, the Marathas, and the Mysores. Science, engineering, art, literature, astronomy, and philosophy flourished under the patronage of these kings.[citation needed]

Northeast Asia
Main articles: History of East Asia and History of Siberia
After a period of relative disunity, China was reunified by the Sui dynasty in 581[citation needed] and under the succeeding Tang dynasty (618–907) China entered a Golden Age.[107] The Tang Empire competed with the Tibetan Empire for control of areas in Inner and Central Asia.[108] The Tang dynasty eventually splintered, however, and after half a century of turmoil the Song dynasty reunified China,[citation needed] when it was, according to William McNeill, the "richest, most skilled, and most populous country on earth".[109] Pressure from nomadic empires to the north became increasingly urgent. By 1142, North China had been lost to the Jurchens in the Jin–Song Wars, and the Mongol Empire[110] conquered all of China in 1279, along with almost half of Eurasia's landmass. After about a century of Mongol Yuan dynasty rule, the ethnic Chinese reasserted control with the founding of the Ming dynasty (1368).


Battle during 1281 Mongol invasion of Japan
In Japan, the imperial lineage had been established by this time, and during the Asuka period (538–710) the Yamato Province developed into a clearly centralized state.[111] Buddhism was introduced, and there was an emphasis on the adoption of elements of Chinese culture and Confucianism. The Nara period of the 8th century[112] marked the emergence of a strong Japanese state and is often portrayed as a golden age.[citation needed] During this period, the imperial government undertook great public works, including government offices, temples, roads, and irrigation systems.[citation needed] The Heian period (794 to 1185) saw the peak of imperial power, followed by the rise of militarized clans, and the beginning of Japanese feudalism. The feudal period of Japanese history, dominated by powerful regional lords (daimyōs) and the military rule of warlords (shōguns) such as the Ashikaga shogunate and Tokugawa shogunate, stretched from 1185 to 1868. The emperor remained, but mostly as a figurehead, and the power of merchants was weak.

Postclassical Korea saw the end of the Three Kingdoms era, the three kingdoms being Goguryeo, Baekje and Silla. Silla conquered Baekje in 660, and Goguryeo in 668,[113] marking the beginning of the North–South States Period (남북국시대), with Unified Silla in the south and Balhae, a successor state to Goguryeo, in the north.[114] In 892 CE, this arrangement reverted to the Later Three Kingdoms, with Goguryeo (then called Taebong and eventually named Goryeo) emerging as dominant, unifying the entire peninsula by 936.[115] The founding Goryeo dynasty ruled until 1392, succeeded by the Joseon dynasty, which ruled for approximately 500 years.

Southeast Asia

Angkor Wat temple, Cambodia, early 12th century
Main article: History of Southeast Asia
The beginning of the Middle Ages in Southeast Asia saw the fall (550 CE) of the Kingdom of Funan to the Chenla Empire, which was then replaced by the Khmer Empire (802 CE). The Khmer people's capital city, Angkor, was the largest city in the world prior to the industrial age and contained over a thousand temples, the most famous being Angkor Wat.

The Sukhothai (1238 CE) and Ayutthaya (1351 CE) kingdoms were major powers of the Thai people, who were influenced by the Khmer.

Starting in the 9th century, the Pagan Kingdom rose to prominence in modern Myanmar. Its collapse brought about political fragmention that ended with the rise of the Toungoo Empire in the 16th century.

Other notable kingdoms of the period include the Srivijayan Empire and the Lavo Kingdom (both coming into prominence in the 7th century), the Champa and the Hariphunchai (both about 750), the Đại Việt (968), Lan Na (13th century), Majapahit (1293), Lan Xang (1354), and the Kingdom of Ava (1364).

This period saw the spread of Islam to present-day Indonesia (beginning in the 13th century) and the emergence of the Malay states, including the Malacca Sultanate and the Bruneian Empire.

In the Philippines, several polities arose during this period, including the Rajahnate of Maynila, the Rajahnate of Cebu, the Rajahnate of Butuan.

Oceania

Moai, Rapa Nui (Easter Island)
Main article: History of Oceania
In the region of Oceania, the Tuʻi Tonga Empire was founded in the 10th century CE and expanded between 1200 and 1500. Tongan culture, language, and hegemony spread widely throughout Eastern Melanesia, Micronesia, and Central Polynesia during this period,[116] influencing East 'Uvea, Rotuma, Futuna, Samoa, and Niue, as well as specific islands and parts of Micronesia (Kiribati, Pohnpei, and miscellaneous outliers), Vanuatu, and New Caledonia (specifically, the Loyalty Islands, with the main island being predominantly populated by the Melanesian Kanak people and their cultures).[117]

At around the same time, a powerful thalassocracy appeared in Eastern Polynesia, centered around the Society Islands, specifically on the sacred Taputapuatea marae, which drew in Eastern Polynesian colonists from places as far away as Hawaii, New Zealand (Aotearoa), and the Tuamotu Islands for political, spiritual and economic reasons, until the unexplained collapse of regular long-distance voyaging in the Eastern Pacific a few centuries before Europeans began exploring the area.

Indigenous written records from this period are virtually nonexistent, as it seems that all Pacific Islanders, with the possible exception of the enigmatic Rapa Nui and their currently undecipherable Rongorongo script, had no writing systems of any kind until after their introduction by European colonists. However, some indigenous prehistories can be estimated and academically reconstructed through careful, judicious analysis of native oral traditions, colonial ethnography, archeology, physical anthropology, and linguistics research.

Americas

Machu Picchu, Inca Empire, Peru
Main articles: History of the Americas, History of North America, History of Central America, History of the Caribbean, and History of South America
In North America, this period saw the rise of the Mississippian culture in the modern-day United States c. 800 CE, marked by the extensive 12th-century urban complex at Cahokia. The Ancestral Puebloans and their predecessors (9th – 13th centuries) built extensive permanent settlements, including stone structures that would remain the largest buildings in North America until the 19th century.[118]

In Mesoamerica, the Teotihuacan civilization fell and the Classic Maya collapse occurred. The Aztec Empire came to dominate much of Mesoamerica in the 14th and 15th centuries.

In South America, the 14th and 15th centuries saw the rise of the Inca. The Inca Empire of Tawantinsuyu, with its capital at Cusco, spanned the entire Andes, making it the most extensive pre-Columbian civilization. The Inca were prosperous and advanced, known for an excellent road system and unrivaled masonry.

Modern history
"Modern Age" redirects here. For the periodical, see Modern Age (periodical).
See also: Timelines of modern history

Gutenberg Bible, ca. 1450, produced using movable type
In the linear, global, historiographical approach, modern history (the "modern period," the "modern era," "modern times") is the history of the period following post-classical history (in Europe known as the "Middle Ages"), spanning from about 1500 to the present. "Contemporary history" includes events from around 1945 to the present. (The definitions of both terms, "modern history" and "contemporary history", have changed over time, as more history has occurred, and so have their start dates.)[119][120] Modern history can be further broken down into periods:

The early modern period began around 1500 and ended around 1815. Notable historical milestones included the continued European Renaissance (whose start is dated variously between 1200 and 1401), the Age of Exploration, the Islamic gunpowder empires, the Protestant Reformation,[121][122] and the American Revolution. With the Scientific Revolution, new information about the world was discovered via empirical observation[123] and the scientific method, by contrast with the earlier emphasis on reason and "innate knowledge". The Scientific Revolution received impetus from Johannes Gutenberg's introduction to Europe of printing, using movable type, and from the invention of the telescope and microscope. Globalization was fuelled by international trade and colonization.
The late modern period began sometime around 1750–1815, as Europe experienced the Industrial Revolution and the military-political turbulence of the French Revolution and the Napoleonic Wars, which were followed by the Pax Britannica. The late modern period continues either to the end of World War II, in 1945, or to the present. Other notable historical milestones included the Great Divergence and the Russian Revolution.
Contemporary history (a period also dubbed Pax Americana in geopolitics) includes historic events from approximately 1945 that are closely relevant to the present time. Major developments include the Cold War, continual hot wars and proxy wars, the Jet Age, the DNA revolution, the Green Revolution,[b] artificial satellites and global positioning systems (GPS), development of the supranational European Union, the Information Age, rapid economic development in India and China, increasing terrorism, and a daunting array of global ecological crises headed by the imminent existential threat of runaway global warming.
The defining features of the modern era developed predominantly in Europe, and so different periodizations are sometimes applied to other parts of the world. When the European periods are used globally, this is often in the context of contact with European culture in the Age of Discovery.[125]

In the humanities and social sciences, the norms, attitudes, and practices arising during the modern period are known as modernity. The corresponding terms for post-World War II culture are postmodernity or late modernity.

Early modern period
Main article: Early modern period
The "Early Modern period"[c] was the period between the Middle Ages and the Industrial Revolution—roughly 1500 to 1800.[20] The Early Modern period was characterized by the rise of science, and by increasingly rapid technological progress, secularized civic politics, and the nation state. Capitalist economies began their rise, initially in northern Italian republics such as Genoa. The Early Modern period saw the rise and dominance of mercantilist economic theory, and the decline and eventual disappearance, in much of the European sphere, of feudalism, serfdom, and the power of the Catholic Church. The period included the Protestant Reformation, the disastrous Thirty Years' War, the Age of Exploration, European colonial expansion, the peak of European witch-hunting, the Scientific revolution, and the Age of Enlightenment.[d]

Renaissance

Leonardo da Vinci's Vitruvian Man (c. 1490), Renaissance Italy
Main article: Renaissance
Europe's Renaissance – the "rebirth" of classical culture, beginning in the 14th century and extending into the 16th – comprised the rediscovery of the classical world's cultural, scientific, and technological achievements, and the economic and social rise of Europe.

The Renaissance engendered a culture of inquisitiveness which ultimately led to Humanism[126] and the Scientific Revolution.[127]

This period, which saw social and political upheavals, and revolutions in many intellectual pursuits, is also celebrated for its artistic developments and the attainments of such polymaths as Leonardo da Vinci and Michelangelo, who inspired the term "Renaissance man."

European expansion
Further information: Age of Discovery, Colonialism, 16th century, and 17th century
During this period, European powers came to dominate most of the world. Although the most developed regions of European classical civilization were more urbanized than any other region of the world, European civilization had undergone a lengthy period of gradual decline and collapse. During the Early Modern Period, Europe was able to regain its dominance; historians still debate the causes.

Europe's success in this period stands in contrast to other regions. For example, one of the most advanced civilizations of the Middle Ages was China. It had developed an advanced monetary economy by 1000 CE. China had a free peasantry who were no longer subsistence farmers, and could sell their produce and actively participate in the market. According to Adam Smith, writing in the 18th century, China had long been one of the richest, most fertile, best cultivated, most industrious, most urbanized, and most prosperous countries in the world. It enjoyed a technological advantage and had a monopoly in cast iron production, piston bellows, suspension bridge construction, printing, and the compass. However, it seemed to have long since stopped progressing. Marco Polo, who visited China in the 13th century, describes its cultivation, industry, and populousness almost in the same terms as travellers would in the 18th century.

One theory of Europe's rise holds that Europe's geography played an important role in its success. The Middle East, India and China are all ringed by mountains and oceans but, once past these outer barriers, are nearly flat. By contrast, the Pyrenees, Alps, Apennines, Carpathians and other mountain ranges run through Europe, and the continent is also divided by several seas. This gave Europe some degree of protection from the peril of Central Asian invaders. Before the era of firearms, these nomads were militarily superior to the agricultural states on the periphery of the Eurasian continent and, as they broke out into the plains of northern India or the valleys of China, were all but unstoppable. These invasions were often devastating. The Golden Age of Islam was ended by the Mongol sack of Baghdad in 1258. India and China were subject to periodic invasions, and Russia spent a couple of centuries under the Mongol-Tatar yoke. Central and western Europe, logistically more distant from the Central Asian heartland, proved less vulnerable to these threats.

Geography contributed to important geopolitical differences. For most of their histories, China, India, and the Middle East were each unified under a single dominant power that expanded until it reached the surrounding mountains and deserts.[citation needed] In 1600 the Ottoman Empire controlled almost all the Middle East,[128] the Ming dynasty ruled China,[129][130] and the Mughal Empire held sway over India. By contrast, Europe was almost always divided into a number of warring states. Pan-European empires, with the notable exception of the Roman Empire, tended to collapse soon after they arose. Another doubtless important geographic factor in the rise of Europe was the Mediterranean Sea, which, for millennia, had functioned as a maritime superhighway fostering the exchange of goods, people, ideas and inventions.

Nearly all the agricultural civilizations have been heavily constrained by their environments. Productivity remained low, and climatic changes easily instigated boom-and-bust cycles that brought about civilizations' rise and fall. By about 1500, however, there was a qualitative change in world history. Technological advance and the wealth generated by trade gradually brought about a widening of possibilities.[131][contradictory](see see here - (self promotion?))


1570 world map, showing Europeans' discoveries
Many have also argued that Europe's institutions allowed it to expand, that property rights and free-market economics were stronger than elsewhere due to an ideal of freedom peculiar to Europe. In recent years, however, scholars such as Kenneth Pomeranz have challenged this view. Europe's maritime expansion unsurprisingly—given the continent's geography—was largely the work of its Atlantic states: Portugal, Spain, England, France, and the Netherlands. Initially the Portuguese and Spanish Empires were the predominant conquerors and sources of influence, and their union resulted in the Iberian Union, the first global empire on which the "sun never set". Soon the more northern English, French and Dutch began to dominate the Atlantic. In a series of wars fought in the 17th and 18th centuries, culminating with the Napoleonic Wars, Britain emerged as the new world power.

Regional developments

Hagia Sophia, Istanbul (formerly Constantinople), Turkey
Persia came under the rule of the Safavid Empire in 1501, succeeded by the Afsharid Empire in 1736, the Zand Empire in 1751, and the Qajar Empire in 1794. Areas to the north and east in Central Asia were held by Uzbeks and Pashtuns. The Ottoman Empire, after taking Constantinople in 1453, quickly gained control of the Middle East, the Balkans, and most of North Africa.

In Africa, this period saw a decline in many civilizations and an advancement in others. The Swahili Coast declined after coming under the Portuguese Empire and later the Omani Empire. In West Africa, the Songhai Empire fell to the Moroccans in 1591 when they invaded with guns.The Bono State which gave birth to numerous Akan states in search of gold such as Akwamu, Akyem, Fante, Adanse etc.[132] The South African Kingdom of Zimbabwe gave way to smaller kingdoms such as Mutapa, Butua, and Rozvi. Ethiopia suffered from the 1531 invasion from neighbouring Muslim Adal Sultanate, and in 1769 entered the Zemene Mesafint (Age of Princes) during which the Emperor became a figurehead and the country was ruled by warlords, though the royal line later would recover under Emperor Tewodros II. The Ajuran Sultanate, in the Horn of Africa, began to decline in the 17th century, succeeded by the Geledi Sultanate. Other civilizations in Africa advanced during this period. The Oyo Empire experienced its golden age, as did the Kingdom of Benin. The Ashanti Empire rose to power in what is modern day Ghana in 1670. The Kingdom of Kongo also thrived during this period.


Ming Dynasty section, Great Wall of China
In China, the Ming gave way in 1644 to the Qing, the last Chinese imperial dynasty, which would rule until 1912. Japan experienced its Azuchi–Momoyama period (1568–1603), followed by the Edo period (1603–1868). The Korean Joseon dynasty (1392–1910) ruled throughout this period, successfully repelling 16th- and 17th-century invasions from Japan and China. Japan and China were significantly affected during this period by expanded maritime trade with Europe, particularly the Portuguese in Japan. During the Edo period, Japan would pursue isolationist policies, to eliminate foreign influences.


Taj Mahal, Mughal Empire, India
On the Indian subcontinent, the Delhi Sultanate and the Deccan sultanates would give way, beginning in the 16th century, to the Mughal Empire.[citation needed] Starting in the northwest, the Mughal Empire would by the late 17th century come to rule the entire subcontinent,[133] except for the southernmost Indian provinces, which would remain independent. Against the Muslim Mughal Empire, the Hindu Maratha Empire was founded on the west coast in 1674, gradually gaining territory—a majority of present-day India—from the Mughals over several decades, particularly in the Mughal–Maratha Wars (1681–1701). The Maratha Empire would in 1818 fall under the control of the British East India Company, with all former Maratha and Mughal authority devolving in 1858 to the British Raj.

In 1511 the Portuguese overthrew the Malacca Sultanate in present-day Malaysia and Indonesian Sumatra. The Portuguese held this important trading territory (and the valuable associated navigational strait) until overthrown by the Dutch in 1641. The Johor Sultanate, centred on the southern tip of the Malay Peninsula, became the dominant trading power in the region. European colonization expanded with the Dutch in the Netherlands East Indies, the Portuguese in East Timor, and the Spanish in the Philippines. Into the 19th century, European expansion would affect the whole of Southeast Asia, with the British in Myanmar and Malaysia and the French in Indochina. Only Thailand would successfully resist colonization.

The Pacific islands of Oceania would also be affected by European contact, starting with the circumnavigational voyage of Ferdinand Magellan, who landed on the Marianas and other islands in 1521. Also notable were the voyages (1642–44) of Abel Tasman to present-day Australia, New Zealand and nearby islands, and the voyages (1768–1779) of Captain James Cook, who made the first recorded European contact with Hawaii. Britain would found its first colony on Australia in 1788.

In the Americas, the western European powers vigorously colonized the newly discovered continents, largely displacing the indigenous populations, and destroying the advanced civilizations of the Aztecs and the Incas. Spain, Portugal, Britain, and France all made extensive territorial claims, and undertook large-scale settlement, including the importation of large numbers of African slaves. Portugal claimed Brazil. Spain claimed the rest of South America, Mesoamerica, and southern North America. Britain colonized the east coast of North America, and France colonized the central region of North America. Russia made incursions onto the northwest coast of North America, with a first colony in present-day Alaska in 1784, and the outpost of Fort Ross in present-day California in 1812.[134] In 1762, in the midst of the Seven Years' War, France secretly ceded most of its North American claims to Spain in the Treaty of Fontainebleau. Thirteen of the British colonies declared independence as the United States of America in 1776, ratified by the Treaty of Paris in 1783, ending the American Revolutionary War. Napoleon Bonaparte won France's claims back from Spain in the Napoleonic Wars in 1800, but sold them to the United States in 1803 as the Louisiana Purchase.

In Russia, Ivan the Terrible was crowned in 1547 as the first Tsar of Russia, and by annexing the Turkic khanates in the east, transformed Russia into a regional power. The countries of western Europe, while expanding prodigiously through technological advancement and colonial conquest, competed with each other economically and militarily in a state of almost constant war. Often the wars had a religious dimension, either Catholic versus Protestant, or (primarily in eastern Europe) Christian versus Muslim. Wars of particular note include the Thirty Years' War, the War of the Spanish Succession, the Seven Years' War, and the French Revolutionary Wars. Napoleon came to power in France in 1799, an event foreshadowing the Napoleonic Wars of the early 19th century.

Late modern period
1750–1914
Main article: Late modern period
Further information: 18th century, 19th century, Long nineteenth century, Age of Imperialism, Age of Revolution, Diplomatic Revolution, and Industrial Revolution

Watt's steam engine powered the Industrial Revolution.
The Scientific Revolution changed humanity's understanding of the world and led to the Industrial Revolution, a major transformation of the world's economies. The Scientific Revolution in the 17th century had had little immediate effect on industrial technology; only in the second half of the 18th century did scientific advances begin to be applied substantially to practical invention. The Industrial Revolution began in Great Britain and used new modes of production—the factory, mass production, and mechanization—to manufacture a wide array of goods faster and using less labour than previously required. The Age of Enlightenment also led to the beginnings of modern democracy in the late-18th century American and French Revolutions. Democracy and republicanism would grow to have a profound effect on world events and on quality of life.


Empires, 1898

The Wright Brothers built and flew the first airplane, the Wright Flyer, in 1903
After Europeans had achieved influence and control over the Americas, imperial activities turned to the lands of Asia and Oceania. In the 19th century the European states had social and technological advantage over Eastern lands.[citation needed] Britain gained control of the Indian subcontinent, Egypt and the Malay Peninsula; the French took Indochina; while the Dutch cemented their control over the Dutch East Indies. The British also colonized Australia, New Zealand and South Africa with large numbers of British colonists emigrating to these colonies. Russia colonized large pre-agricultural areas of Siberia. In the late 19th century, the European powers divided the remaining areas of Africa. Within Europe, economic and military challenges created a system of nation states, and ethno-linguistic groupings began to identify themselves as distinctive nations with aspirations for cultural and political autonomy. This nationalism would become important to peoples across the world in the 20th century.

During the Second Industrial Revolution, the world economy became reliant on coal as a fuel, as new methods of transport, such as railways and steamships, effectively shrank the world. Meanwhile, industrial pollution and environmental damage, present since the discovery of fire and the beginning of civilization, accelerated drastically.

The advantages that Europe had developed by the mid-18th century were two: an entrepreneurial culture,[135] and the wealth generated by the Atlantic trade (including the African slave trade). By the late 16th century, silver from the Americas accounted for the Spanish empire's wealth.[citation needed] The profits of the slave trade and of West Indian plantations amounted to 5% of the British economy at the time of the Industrial Revolution.[136] While some historians conclude that, in 1750, labour productivity in the most developed regions of China was still on a par with that of Europe's Atlantic economy,[137] other historians such as Angus Maddison hold that the per-capita productivity of western Europe had by the late Middle Ages surpassed that of all other regions.[138]

1914–1945
Main article: 20th century
Further information: Interwar period, Roaring Twenties, and Great Depression

World War I trench warfare
The 20th century opened with Europe at an apex of wealth and power, and with much of the world under its direct colonial control or its indirect domination. Much of the rest of the world was influenced by heavily Europeanized nations: the United States and Japan.

As the century unfolded, however, the global system dominated by rival powers was subjected to severe strains, and ultimately yielded to a more fluid structure of independent nations organized on Western models.

This transformation was catalyzed by wars of unparalleled scope and devastation. World War I led to the collapse of four empires – Austria-Hungary, the German Empire, the Ottoman Empire, and the Russian Empire – and weakened Great Britain and France.

In the war's aftermath, powerful ideologies rose to prominence. The Russian Revolution of 1917 created the first communist state, while the 1920s and 1930s saw militaristic fascist dictatorships gain control in Italy, Germany, Spain, and elsewhere.


Atomic bombings: Hiroshima, Nagasaki, 1945
Ongoing national rivalries, exacerbated by the economic turmoil of the Great Depression, helped precipitate World War II. The militaristic dictatorships of Europe and Japan pursued an ultimately doomed course of imperialist expansionism, in the course of which Nazi Germany orchestrated the murder of six million Jews in the Holocaust, while Imperial Japan murdered millions of Chinese.

The World War II defeat of the Axis Powers opened the way for the advance of communism into Central Europe, Yugoslavia, Bulgaria, Romania, Albania, China, North Vietnam, and North Korea.

Contemporary history
Main article: Contemporary history
1945–2000
Main article: 20th century
Further information: Cold War, Green Revolution, Space exploration, and Digital Revolution

Civilians (here, Mỹ Lai, Việt Nam, 1968) suffered greatly in 20th-century wars.
When World War II ended in 1945, the United Nations was founded in the hope of preventing future wars,[139] as the League of Nations had been formed following World War I.[140] The war had left two countries, the United States and the Soviet Union, with principal power to influence international affairs.[141] Each was suspicious of the other and feared a global spread of the other's, respectively capitalist and communist, political-economic model. This led to the Cold War, a forty-five-year stand-off and arms race between the United States and its allies, on one hand, and the Soviet Union and its allies on the other.[142]

With the development of nuclear weapons during World War II, and with their subsequent proliferation, all of humanity were put at risk of nuclear war between the two superpowers, as demonstrated by many incidents, most prominently the October 1962 Cuban Missile Crisis. Such war being viewed as impractical, the superpowers instead waged proxy wars in non-nuclear-armed Third World countries.[143]

In China, Mao Zedong implemented industrialization and collectivization reforms as part of the Great Leap Forward (1958–1962), leading to the starvation deaths (1959–1961) of tens of millions of people.

Between 1969 and 1972, as part of the Cold War space race, twelve men landed on the Moon and safely returned to Earth.[e]

The Cold War ended in 1991, when the Soviet Union disintegrated, in part due to inability to compete economically with the United States and western Europe. However, the United States likewise began to show signs of slippage in its geopolitical influence,[145][f] even as its private sector, now less inhibited by the claims of the public sector, increasingly sought private advantage to the prejudice of the public weal.[g][h][i]

In the early postwar decades, the colonies in Asia and Africa of the Belgian, British, Dutch, French, and other west European empires won their formal independence.[150] However, these newly independent countries often faced challenges in the form of neocolonialism, sociopolitical disarray, poverty, illiteracy, and endemic tropical diseases.[151][j][k]

Most Western European and Central European countries gradually formed a political and economic community, the European Union, which expanded eastward to include former Soviet-satellite countries.[154][155][156] The European Union's effectiveness was handicapped by the immaturity of its common economic and political institutions,[l] somewhat comparable to the inadequacy of United States institutions under the Articles of Confederation prior to the adoption of the U.S. Constitution that came into force in 1789. Asian, African, and South American countries followed suit and began taking tentative steps toward forming their own respective continental associations.


Last Moon landing: Apollo 17 (1972)
Cold War preparations to deter or to fight a third world war accelerated advances in technologies that, though conceptualized before World War II, had been implemented for that war's exigencies, such as jet aircraft, rocketry, and electronic computers. In the decades after World War II, these advances led to jet travel, artificial satellites with innumerable applications including global positioning systems (GPS), and the Internet—inventions that have revolutionized the movement of people, ideas, and information.

However, not all scientific and technological advances in the second half of the 20th century required an initial military impetus. That period also saw ground-breaking developments such as the discovery of the structure of DNA,[158] the consequent sequencing of the human genome, the worldwide eradication of smallpox, the discovery of plate tectonics, manned and unmanned exploration of space and of previously inaccessible parts of Earth, and foundational discoveries in physics phenomena ranging from the smallest entities (particle physics) to the greatest entity (physical cosmology).

21st century
Main article: 21st century

Steve Jobs discussing the iPhone, an early smartphone, in 2008
The 21st century has been marked by growing economic globalization and integration, with consequent increased risk to interlinked economies, as exemplified by the Great Recession of the late 2000s and early 2010s.[159] This period has also seen the expansion of communications with mobile phones and the Internet, which have caused fundamental societal changes in business, politics, and individuals' personal lives.

Worldwide competition for resources has risen due to growing populations and industrialization, especially in India, China, and Brazil. The increased demands are contributing to increased environmental degradation and to global warming.

The early 21st century saw escalating intra- and international strife in the Near East and Afghanistan, stimulated by vast economic disparities, by dissatisfaction with governments dominated by Western interests, by inter-ethnic and inter-sectarian feuds, and by the longest war in the history of the United States, the proximate cause for which was Osama bin Laden's provocative 2001 destruction of New York City's World Trade Center, leading to the Iraq War and the War on Terror. The Arab Spring, a revolutionary wave of uprisings in North Africa and the Near East in the early 2010s, produced power vacuums that led to a resurgence of authoritarianism and the advent of reactionary groups like the Islamic State.


China has urbanized rapidly in the 21st century (Shanghai pictured).
U.S. military involvements in the Near East and Afghanistan, along with a financial crisis and resultant recession, have drained U.S. economic resources at a time when the U.S. and other Western countries are experiencing mounting socioeconomic dislocations aggravated by the robotization of work and the export of industries to cheaper-workforce countries.[m][n] Meanwhile, ancient and populous Asian civilizations – India and especially China – have been emerging from centuries of relative scientific, technological, and economic dormancy to become potential economic and political rivals for Western powers.[161]

International tensions were heightened in connection with the efforts of some nuclear-armed states to induce North Korea to give up its nuclear weapons, and to prevent Iran from developing nuclear weapons.[162]

See also
icon	Modern history portal
Andrew Marr's History of the World (2012 BBC series)
Cultural history
Economic history of the world
Globalization
Historic recurrence
Historiography
History of science
History of technology
List of archeological periods
List of millennia
List of time periods
Timeline of historical geopolitical changes
Western Civilization
Explanatory notes
 The very word "civilization" comes from the Latin civilis, meaning "civil," related to civis ("citizen") and civitas ("city" or "city-state").[49]
 However, the Green Revolution has brought unintended consequences: "India originally possessed some 110,000 landraces of rice with diverse and valuable properties. These include enrichment in vital nutrients and the ability to withstand flood, drought, salinity or pest infestations. The Green Revolution covered fields with a few high-yielding varieties, so that roughly 90 percent of the landraces vanished from farmers' collections. High-yielding varieties require expensive inputs. They perform abysmally on marginal farms or in adverse environmental conditions, forcing poor farmers into debt."[124]
 "Early Modern," historically speaking, refers to Western European history from 1501 (after the widely accepted end of the Late Middle Ages; the transition period was the 15th century) to either 1750 or c. 1790–1800, by whichever epoch is favoured by a school of scholars defining the period—which, in many cases of periodization, differs as well within a discipline such as art, philosophy or history.
 The Age of Enlightenment has also been referred to as the Age of Reason. Historians also include the late 17th century, which is typically known as the Age of Reason or Age of Rationalism, as part of the Enlightenment; however, contemporary historians have considered the Age of Reason distinct to the ideas developed in the Enlightenment. The use of the term here includes both Ages under a single all-inclusive time-frame.
 James Gleick writes in The New York Review of Books: "'If we can put a man on the moon, why can's we...?' became a cliché even before [the] Apollo [program] succeeded.... Now... the missing predicate is the urgent one: why can't we stop destroying the climate of our own planet?... I say leave it [the moon] alone for a while."[144]
 "In the aftermath of the disintegration of the Soviet Union..." writes Graham Allison, "Americans were... caught up in a surge of triumphalism." Francis Fukuyama, in a 1992 best-selling book, proclaimed The End of History, the victory of free-market economics, and the permanent ascendancy of Western liberal democracy. But it soon became evident, writes Allison, that "the end of the Cold War [had] produced a unipolar moment, not a unipolar era. [T]he U.S. economy, which [had] accounted for half of the world's GDP after World War II, had fallen to less than a quarter of global GDP by the end of the Cold War and stands at just one-seventh today. For a nation whose core strategy has been to overwhelm challenges with resources, this decline calls into question the terms of U.S. leadership."[146]
 "In the advanced economies of the West, from 1945 to around 1975," writes Robin Varghese in Foreign Affairs, "voters showed how politics could tame markets, putting officials in power who pursued a range of social democratic policies without damaging the economy. This period... saw a historically unique combination of high growth, increasing productivity, rising real wages, technological innovation, and expanding systems of social insurance in Western Europe, North America, and Japan.... Since the 1970s, businesses across the developed world have been cutting their wage bills not only through labor-saving technological innovations but also by pushing for regulatory changes and developing new forms of employment. These include just-in-time contracts, which shift risks to workers; noncompete clauses, which reduce bargaining power; and freelance arrangements, which exempt businesses from providing employees with benefits such as health insurance. The result has been that since the beginning of the twenty-first century, labor's share of GDP has fallen steadily in many developed economies.... The challenge today is to identify... a mixed economy that can successfully deliver what the [1945–75] golden age did, this time with greater gender and racial equality to boot."[147]
 Historian Christopher R. Browning writes: "In the first three postwar decades, workers and management effectively shared the increased wealth produced by the growth in productivity. Since the 1970s that social contract has collapsed, union membership and influence have declined, wage growth has stagnated, and inequality in wealth has grown sharply."[148]
 Economics Nobel laureate Joseph E. Stiglitz writes in Scientific American, in part: "[T]he U.S. has the highest level of economic inequality among developed countries.... Since the mid-1970s the rules of the economic game have been rewritten... globally and nationally [to] advantage the rich... in a political system that is itself rigged through gerrymandering, voter suppression and the influence of money.... [Enforcement of] antitrust laws, first enacted [in 1890] in the U.S. to prevent the agglomeration of market power, has weakened... Technological changes have concentrated market power in the hands of a few global players... part[ly] because of "network effects"... [E]stablished firms with deep war chests have enormous power to crush competitors and ultimately raise prices.... A concerted attack on unions has almost halved the fraction of unionized workers in the [U.S.], to about 11 percent.... U.S. investment treaties such as NAFTA protect investors against a tightening of environmental and health regulations abroad. [Such] provisions... enhance the credibility of a company's threat to move abroad if workers do not temper their demands.... [I]t is hard to imagine meaningful change without a concerted effort to take money out of politics..."[149]
 The president of the World Bank, Jim Yong Kim, urges the governments of both developed and developing countries to invest more in human capital, "which is the sum total of a population's health, skills, knowledge, experience, and habits." Increased levels of quality education increase a person's income. "Socioemotional skills, such as grit and conscientiousness, often have equally large economic returns.... Health also matters. [I]n Kenya, [administration of inexpensive] deworming drugs in childhood [has] reduced school absences and raised wages in adulthood by... 20 percent... Proper nutrition and stimulation in utero and during early childhood improve physical and mental well-being later in life. [F]ocusing on human capital during the first 1,000 days of a child's life is one of the most cost-effective investments governments can make.... Human capital doesn't materialize on its own; it must be nurtured by the state."[152]
 William Hardy McNeill, in his 1963 book The Rise of the West, appears to have interpreted the decline of the European empires as paradoxically being due to Westernization itself, writing that "Although European empires have decayed since 1945, and the separate nation-states of Europe have been eclipsed as centers of political power by the melding of peoples and nations occurring under the aegis of both the American and Russian governments, it remains true that, since the end of World War II, the scramble to imitate and appropriate science, technology, and other aspects of Western culture has accelerated enormously all round the world. Thus the dethronement of western Europe from its brief mastery of the globe coincided with (and was caused by) an unprecedented, rapid Westernization of all the peoples of the earth."[153]:566 McNeill further writes that "The rise of the West, as intended by the title and meaning of this book, is only accelerated when one or another Asian or African people throws off European administration by making Western techniques, attitudes, and ideas sufficiently their own to permit them to do so".[153]:807
 James McAuley writes in The New York Review of Books, 15 August 2019, pp. 47–48: "There was never a single moment that marked the definitive establishment of the European Union, which... has continued to define itself since World War II. [T]he major turning points have all been quiet steps on the way to further economic integration while preserving national sovereignty. Today there is only an incomplete monetary union without a real political contract to manage it... [Nevertheless, the Union's] various peoples have grown remarkably closer... The European Union now has open borders, a single market from Portugal to the Baltics, and more or less monthly meetings of member state leaders [the European Council]. What's more, those member states are now closer to each other than they are to the United States... [T]his transformation has occurred informally and organically... [R]obust supranational politics are taking root in Europe... Luuk van Middelaar writes: '[W]hat unites us as Europeans on this continent is bigger and stronger than anything that divides us.'"[157]
 Liaquat Ahamed writes in The New Yorker, 2 September 2019, p. 28: "As the world economy opened up in the nineteen-eighties, newly mobile capital tended to flow to places that offered the highest return, and very often these were countries with the lowest taxes and the least onerous regulation. To hold on to capital, countries found themselves forced to match the free-market policies of their trading partners. [T]his shift, in turn, led to more uneven income distributions. Countries with larger tax cuts experienced bigger increases in inequality."[160]
 Liaquat Ahamed writes in The New Yorker, 2 September 2019, p. 28: "[T]here seems to [be] some sort of cap on inequality – a limit to the economic divisions a country can ultimately cope with."[160]
References
 "International Programs - Historical Estimates of World Population - U.S. Census Bureau". United States Census Bureau. August 2016. Archived from the original on 9 July 2012. Retrieved 15 November 2016.
 Tudge 1998, pp. 30–31.
 Muscarella, Oscar White (1 January 2013), "Jiroft and "Jiroft-Aratta": A Review Article of Yousef Madjidzadeh, Jiroft: The Earliest Oriental Civilization", Archaeology, Artifacts and Antiquities of the Ancient Near East, BRILL, pp. 485–522, ISBN 978-90-04-23669-1, retrieved 8 July 2020
 Muscarella, Oscar White. (2013). Archaeology, artifacts and antiquities of the ancient Near East : sites, cultures, and proveniences. Brill. ISBN 978-90-04-23669-1. OCLC 848917597.
 Maǧīdzāda, Y. (2003). Jiroft: The earliest oriental civilization. Tehran: Organization of the Ministry of Culture ans Islamic Guidance.
 People, "New evidence: modern civilization began in Iran", 10 Aug 2007, retrieved 1 October 2007
 Xinhua, "New evidence: modern civilization began in Iran", xinhuanet.com, 10 August 2007
 McNeill 1999, pp. 13–15.
 Chakrabarti 2004, p. 11.
 Baines & Malek 2000, p. 8.
 Bard 2000, pp. 64–65.
 Lee 2002, pp. 15–42.
 Teeple 2006, pp. 14–20.
 Roberts & Westad 2013, p. 161.
 Stearns & Langer 2001, p. 12.
 Stearns & Langer 2001, p. 14.
 Hart-Davis 2012, p. 63.
 Grant 2006, p. 53.
 Roberts & Westad 2013, p. 535.
 Bentley & Ziegler 2008, p. 595.
 Roberts & Westad 2013, pp. 712–14.
 Baten 2016, pp. 1–13.
 Chen & Li 2001, pp. 444–56.
 "Homo sapiens". The Smithsonian Institutions's Human Origins Program. Smithsonian Institution. 8 February 2016. Archived from the original on 26 January 2018. Retrieved 21 May 2017.
 Klein, Richard G. (June 1995). "Anatomy, Behavior, and Modern Human Origins". Journal of World Prehistory. 9 (2): 167–98. doi:10.1007/BF02221838. ISSN 0892-7537.
 Stringer, C. (2012). "Evolution: What Makes a Modern Human". Nature. 485 (7396): 33–35. Bibcode:2012Natur.485...33S. doi:10.1038/485033a. PMID 22552077.
 Hart-Davis 2012, pp. 24–29.
 Hart-Davis 2012, p. 17.
 Hart-Davis 2012, pp. 20–21.
 Hart-Davis 2012, pp. 32–33.
 Hart-Davis 2012, pp. 30–31.
 Gavashelishvili, Alexander; Tarkhnishvili, David (2016). "Biomes and human distribution during the last ice age". Global Ecology and Biogeography. 25 (5): 563–74. doi:10.1111/geb.12437.
 Hart-Davis 2012, pp. 36–37.
 McNeill 1999, p. 11.
 Hart-Davis 2012, pp. 42–43.
 McNeill 1999, p. 13.
 Roberts & Westad 2013.
 Roberts & Westad 2013, p. 53.
 Hart-Davis 2012, p. 44.
 Roberts & Westad 2013, p. 59.
 McNeill 1999, p. 16.
 McNeill 1999, p. 18.
 Regulski, Ilona (2 May 2016). "The Origins and Early Development of Writing in Egypt". doi:10.1093/oxfordhb/9780199935413.013.61.
 Wengrow, David. "The Invention of Writing in Egypt", in Before the Pyramids: Origin of Egyptian Civilization, Oriental Institute, University of Chicago, 2011, pp. 99-103.
 James Legge, D.D., translator, "The Shoo King, or the Book of Historical Documents, Volume III, Part I, page 12]. Early Chinese Writing", in The World's Writing Systems, ed. Bright and Daniels, p.191
 Brian M. Fagan, Charlotte Beck, ed. (1996). The Oxford Companion to Archaeology. Oxford University Press. p. 762. ISBN 978-0-19-507618-9.
 Roberts & Westad 2013, pp. 34–35.
 Stearns & Langer 2001, p. 15.
 Larry E. Sullivan (2009), The SAGE glossary of the social and behavioral sciences Archived 30 December 2016 at the Wayback Machine, Editions SAGE, p. 73.
 Stearns & Langer 2001, p. 21.
 Hart-Davis 2012, pp. 54–55.
 Chakrabarti 2004, pp. 10–13.
 Allchin & Allchin 1997, pp. 153–68.
 Roberts & Westad 2013, pp. 43–46.
 Mercer 1949, p. 259.
 Allen 2007, p. 1.
 Buchanan 1979, p. 23.
 Hart-Davis 2012, pp. 62–63.
 Roberts & Westad 2013, pp. 53–54.
 Bard 2000, pp. 57–64.
 Hart-Davis 2012, pp. 76–77.
 McNeill 1999, pp. 36–37.
 Price & Thonemann 2010, p. 22.
 Roberts & Westad 2013, pp. 116–22.
 Singh 2008, pp. 260–64.
 Stearns & Langer 2001, p. 63.
 Stearns & Langer 2001, pp. 70–71.
 Roberts & Westad 2013, p. 110.
 Martin 2000, pp. 106–07.
 Golden 2011, p. 25.
 "Alexander the Great". Historic Figures. BBC. Archived from the original on 19 November 2016. Retrieved 18 November 2016.
 Hemingway, Collette; Hemingway, Seán (April 2007). "Art of the Hellenistic Age and the Hellenistic Tradition". Heilbrunn Timeline of Art History. Metropolitan Museum of Art. Archived from the original on 4 October 2015. Retrieved 18 November 2016.
 Kulke, Hermann; Rothermund, Dietmar (2004). A History of India (4th ed.). Routledge. ISBN 978-0-415-32920-0.
 Nilakanta Sastri, K. A. A History of South India. p. 157.
 Hart-Davis 2012, pp. 106–07.
 Kelly 2007, pp. 4–6.
 Zhou, Jinghao (2003). Remaking China's Public Philosophy for the Twenty-First Century. Westport: Greenwood Publishing Group. ISBN 978-0-275-97882-2.
 Fagan 2005, pp. 390, 396.
 Zapotec civilization has its beginnings in 700 BCE: see Flannery, Kent V.; Marcus, Joyce (1996). Zapotec Civilization: How Urban Society Evolved in Mexico's Oaxaca Valley. New York: Thames & Hudson. p. 146. ISBN 978-0-500-05078-1. Zapotec civilization ended in 1521 according to the five archaeological stages presented in Whitecotton, Joseph W. (1977). The Zapotecs: Princes, Priests, and Peasants. Norman: University of Oklahoma Press. 26, LI.1–3.
 Coe 2011, p. 91.
 Camp, John McK.; Dinsmoor, William B. (1984). Ancient Athenian building methods. Excavations of the Athenian Agora. 21. Princeton, NJ: American School of Classical Studies at Athens. ISBN 978-0-87661-626-0.
 Stearns & Langer 2001, pp. 95, 99.
 Collins 1999, pp. 80–99.
 Collins 1999, pp. 100–15.
 Stearns & Langer 2001, pp. 97, 103.
 Collins 1999, p. 404.
 Loyn 1991, pp. 122–23.
 Whaley, Joachim (2012). Germany and the Holy Roman Empire. 1. pp. 17–20.
 Johnson 1996, p. 23.
 "Dynasties of Early Imperial China: Han Dynasty". Minnesota State University. Archived from the original on 10 July 2009. Retrieved 18 April 2009.
 Gascoigne 2003, pp. 90–92.
 Gernet 1996, pp. 237–38.
 Shaw 1976, p. 13.
 Ebrey, Walthall & Palais 2006, p. 113.
 Xue 1992, pp. 149–52, 257–64.
 Xue 1992, pp. 226–27.
 Ṭabīb et al. 2001, p. 9.
 Stearns & Langer 2001, p. 153.
 Roesdahl, Else (1998). The Vikings. Penguin Books. ISBN 978-0-14-025282-8.
 Dunham, Will (29 January 2008). "Black death 'discriminated' between victims". ABC Science. Archived from the original on 20 December 2016. Retrieved 24 November 2016.
 "De-coding the Black Death". BBC. 3 October 2001. Archived from the original on 7 July 2017. Retrieved 24 November 2016.
 "Plague: The Black Death". National Geographic. Archived from the original on 26 June 2013. Retrieved 3 November 2008.
 Stearns & Langer 2001, p. 280.
 McNeill 1999, pp. 319–23.
 McNeill 1999, pp. 267–68.
 Blier, Suzanne Preston (2012). "Art in Ancient Ife, Birthplace of the Yoruba" (PDF). African Arts. 45 (4): 70–85. doi:10.1162/afar_a_00029. Archived (PDF) from the original on 29 March 2017. Retrieved 24 November 2016.
 Lewis 2009, p. 1.
 Whitfield 2004, p. 193.
 McNeill 1982, p. 50.
 Buell, Paul D. (2003). Historical dictionary of the Mongol world empire. Lanham (Maryland): Scarecrow Press. ISBN 978-0-8108-4571-8.
 Mason, R.H.P.; Caiger, J.G. (2011). A History of Japan (Revised ed.). New York: Tuttle Publishing. ISBN 978-1-4629-0097-8.
 Dolan, Ronald E.; Worden, Robert L., eds. (1994). "Nara and Heian Periods, A.D. 710–1185". Japan: A Country Study. Library of Congress, Federal Research Division.
 Ackerman, Marsha E.; et al., eds. (2008). "Three Kingdoms, Korea". Encyclopedia of world history. New York: Facts on File. p. 464. ISBN 978-0-8160-6386-4.
 "남북국시대 (North-South States Period)". Encyclopedia. Naver. Archived from the original on 10 January 2014. Retrieved 24 November 2016.
 The Association of Korean History Teachers (2005). Korea through the ages; Volume One: Ancient. Seongnam-si: The Center for Information on Korean Culture, The Academy of Korean Studies. p. 113. ISBN 978-89-7105-545-8.
 Kirch, Patrick Vinton; Green, Roger C. (2001). Hawaiki, ancestral Polynesia: an essay in historical anthropology. Cambridge University press. p. 87. ISBN 978-0-521-78879-3.
 Geraghty, Paul (1994). "Linguistic evidence for the Tongan empire". In Dutton, Tom (ed.). Language contact and change in the Austronesian world. Trends in linguistics: Studies and monographs. 77. Berlin: Gruyter. pp. 236–39. ISBN 978-3-11-012786-7.
 Fagan 2005, p. 35.
 Intrinsic to the English language, "modern" denotes (in reference to history) a period that is opposed to either ancient or medieval; modern history is the history of the world since the end of the Middle Ages.
 "The Century Dictionary and Cyclopedia". 1906. Archived from the original on 12 December 2016. Retrieved 15 October 2019.
 Dunan, Marcel (1964). Larousse Encyclopedia of Modern History, From 1500 to the Present Day. New York: Harper & Row. OCLC 395134.
 "modern". The American Heritage Dictionary of the English Language (4th ed.). Houghton Mifflin. 2000. Archived from the original on 22 June 2008. Retrieved 29 November 2019.
 Baird, F.E., & Kaufmann, W.A. (2008). Philosophic classics: From Plato to Derrida. Upper Saddle River, NJ: Pearson/Prentice Hall.
 Debal Deb, "Restoring Rice Biodiversity", Scientific American, vol. 321, no. 4 (October 2019), pp. 54–61. (p. 54.)
 "Islamic Culture and the Medical Arts: Late Medieval and Early Modern Medicine". U.S. National Library of Medicine. National Institutes of Health. 15 December 2011. Archived from the original on 9 October 2019. Retrieved 18 October 2019.
 Hart-Davis 2012, pp. 250–53.
 Roberts & Westad 2013, pp. 683–85.
 Imber 2002, p. 66.
 Ebrey, Walthall & Palais 2006.
 Stearns & Langer 2001, pp. 376–77.
 Miller, Edward; Postan, Cynthia; Postan, Michael Moissey, eds. (1987). The Cambridge economic history of Europe: Volume 2, Trade and Industry in the Middle Ages (2nd ed.). Cambridge: Cambridge University Press. ISBN 978-0-521-08709-4.
 Meyerowitz, Eva L. R. (1975). The Early History of the Akan States of Ghana. Red Candle Press.
 La l, Vinay (2001). "The Mughal Empire". Manas: India and its Neighbors. University of California, Los Angeles. Archived from the original on 30 April 2015. Retrieved 12 April 2015.
 "Fort Ross". Office of Historic Preservation, California State Parks. Retrieved 15 February 2018.
 Wood, Neal (1984). John Locke and agrarian capitalism. Berkeley: University of California Press. ISBN 978-0-520-05046-4.
 Mintz, S.; McNeil, S. "Was slavery the engine of economic growth?". Digital History. Archived from the original on 26 February 2009.
 see the NBER Publications by Carol H. Shiue and Wolfgang Keller at nber.org Archived 28 May 2006 at the Wayback Machine
 "Homepage of Angus Maddison". Ggdc.net. Archived from the original on 28 July 2010. Retrieved 18 April 2009.
 Fasulo 2015, pp. 1–3
 "League of Nations | Definition & Purpose". Encyclopedia Britannica. Archived from the original on 8 July 2017. Retrieved 6 September 2017.
 Zinn, Howard (2003). A People's History of the United States (5th ed.). New York: HarperPerennial Modern Classics [2005 reprint]. ISBN 978-0-06-083865-2.
 Graham Allison, "The Myth of the Liberal Order: From Historical Accident to Conventional Wisdom", Foreign Affairs, vol. 97, no. 4 (July–August 2018), p. 126.
 Graham Allison, "The Myth of the Liberal Order: From Historical Accident to Conventional Wisdom", Foreign Affairs, vol. 97, no. 4 (July–August 2018), pp. 127–28.
 James Gleick, "Moon Fever" [review of Oliver Morton, The Moon: A History of the Future; Apollo's Muse: The Moon in the Age of Photography, an exhibition at the Metropolitan Museum of Art, New York City, 3 July – 22 September 2019; Douglas Brinkley,American Moonshot: John F. Kennedy and the Great Space Race; Brandon R. Brown, The Apollo Chronicles: Engineering America's First Moon Missions; Roger D. Launius, Reaching for the Moon: A Short History of the Space Race; Apollo 11, a documentary film directed by Todd Douglas Miller; and Michael Collins, Carrying the Fire: An Astronaut's Journeys (50th Anniversary Edition)], The New York Review of Books, vol. LXVI, no. 13 (15 August 2019), pp. 54–58. (pp. 57–58.)
 McCormick 1995, p. 155.
 Graham Allison, "The Myth of the Liberal Order: From Historical Accident to Conventional Wisdom", Foreign Affairs, vol. 97, no. 4 (July–August 2018), pp. 129–31.
 Robin Varghese, "Marxist World: What Did You Expect From Capitalism?", Foreign Affairs, vol. 97, no. 4 (July–August 2018), pp. 36–42.
 Christopher R. Browning, "The Suffocation of Democracy", The New York Review of Books, vol. LXV, no. 16 (25 October 2018), p. 16.
 Joseph E. Stiglitz, "A Rigged Economy: And what we can do about it" (The Science of Inequality), Scientific American, vol. 319, no. 5 (November 2018), pp. 57–61.
 Abernethy 2000, p. 133.
 Stern, Nicholas; Rogers, F. Halsey; Dethier, Jean-Jacques (2006). Growth and Empowerment: Making Development Happen. Munich lectures in economics. Cambridge, Mass: MIT Press. ISBN 978-0-262-26474-7.
 Jim Yong Kim, "The Human Capital Gap: Getting Governments to Invest in People", Foreign Affairs, vol. 97, no. 4 (July–August 2018), pp. 92–96.
 McNeill, William H. (1991). The Rise of the West: A History of the Human Community. University of Chicago Press.
 Dinan, Desmond (2004). Europe recast: a history of European Union. Basingstoke: Palgrave Macmillan. ISBN 978-0-333-98734-6.
 Peterson, John; Shackleton, Michael, eds. (2012). The institutions of the European Union (3rd ed.). Oxford University Press. ISBN 978-0-19-957498-8.
 Rifkin, Jeremy (2004). The European dream: how Europe's vision of the future is quietly eclipsing the American dream. New York: Jeremy P. Tarcher. ISBN 978-1-58542-345-3.
 James McAuley, "A More Perfect Union?" (review of Luuk van Middelaar, Alarums and Excursions: Improving Politics on the European Stage, translated from the Dutch by Liz Waters, Agenda, 2019, 301 pp.; and Stéphanie Hennette, Thomas Piketty, Guillaume Sacriste, and Antoine Vauchez, How to Democratize Europe, translated from the French by Paul Dermine, Marc LePain, and Patrick Camiller, Harvard University Press, 2019, 209 pp.), The New York Review of Books, vol. LXVI, no. 13 (15 August 2019), pp. 46–48.
 Clayton 2016, p. 95.
 Bob Davis, "What's a Global Recession?", The Wall Street Journal, 22 April 2009. [1] Archived 28 February 2019 at the Wayback Machine Retrieved 2 January 2019.
 Liaquat Ahamed, "Widening Gyre: The rise and fall and rise of economic inequality", The New Yorker, 2 September 2019, pp. 26–29.
 Paul Kennedy, The Rise and Fall of the Great Powers: Economic Change and Military Conflict from 1500 to 2000, New York, Random House, 1987, ISBN 0-394-54674-1, pp. 242–45, 432, 514–19, 526–29, 533–35, and passim.
 Wendy R. Sherman, "How We Got the Iran Deal: And Why We'll Miss It", Foreign Affairs, vol. 97, no. 5 (September / October 2018), pp. 186–97.
Bibliography
Abernethy, David B. (2000). The Dynamics of Global Dominance : European Overseas Empires, 1415–1980. New Haven, CT: Yale University Press. ISBN 978-0-300-09314-8.
Allchin, Bridget; Allchin, Raymond (1997). Origins of a Civilization: The Prehistory and Early Archaeology of South Asia. New Delhi: Viking. ISBN 978-0-670-87713-3.
Allen, James P. (30 August 2007). Manuelian, Peter D. (ed.). The Ancient Egyptian Pyramid Texts. Society of Biblical Literature. p. 1. ISBN 978-1-58983-678-5.
Allison, Graham, "The Myth of the Liberal Order: From Historical Accident to Conventional Wisdom", Foreign Affairs, vol. 97, no. 4 (July–August 2018), pp. 124–33.
Baines, John; Malek, Jaromir (2000). The Cultural Atlas of Ancient Egypt (revised ed.). Facts on File. ISBN 978-0-8160-4036-0.
Bard, Kathryn A. (2000). "The Emergence of the Egyptian State (c.3200–2686 BC)". In Shaw, Ian (ed.). The Oxford History of Ancient Egypt. Oxford, UK: Oxford University Press. pp. 57–82. ISBN 978-0-19-280458-7.
Baten, Jörg (2016). "Introduction". In Baten, Jörg (ed.). A History of the Global Economy. From 1500 to the Present. Cambridge University Press. pp. 1–13. ISBN 978-1-107-50718-0.
Ben-Ami, Shlomo, "Gobalization's Discontents", The Nation, vol. 307, no. 2 (16 / 23 July 2018), p. 27.
Bentley, Jerry H.; Ziegler, Herbert F. (2008). Traditions & Encounters: A Global Perspective on the Past: Volume II From 1500 to the Present (Fourth ed.). New York: McGraw Hill. ISBN 978-0-07-333063-1.
Browning, Christopher R., "The Suffocation of Democracy", The New York Review of Books, vol. LXV, no. 16 (25 October 2018), pp. 14, 16–17.
Busby, Joshua, "Warming World: Why Climate Change Matters More Than Anything Else", Foreign Affairs, vol. 97, no. 4 (July–August 2018), pp. 49–55.
Buchanan, Robert Angus (1979). "2". History and industrial civilisation. London: Macmillan. ISBN 978-1-349-16128-7.
Chakrabarti, Dilip K. (2004). "Introduction". In Chkrabarti, Dilip K. (ed.). Indus Civilization Sites in India: New Discoveries. Mumbai: Marg Publications. pp. 7–22. ISBN 978-81-85026-63-3.
Chen, F. C.; Li, W. H. (2001). "Genomic Divergences Between Humans and Other Hominoids and the Effective Population Size of the Common Ancestor of Humans and Chimpanzees". American Journal of Human Genetics. 68 (2): 444–56. doi:10.1086/318206. PMC 1235277. PMID 11170892.
Christopher Clark, "'This Is a Reality, Not a Threat'" (review of Lawrence Freedman, The Future of War: A History, Public Affairs, 2018, 376 pp.; and Robert H. Latiff, Future War: Preparing for the New Global Battlefield, Knopf, 2018, 192 pp.), The New York Review of Books, vol. LXV, no. 18 (22 November 2018), pp. 53–54.
Cockburn, Andrew, "How to Start a Nuclear War: The increasingly direct road to ruin", Harper's, vol. 337, no. 2019 (August 2018), pp. 51–58.
Clayton, Julie; Dennis, Carina, eds. (2003). 50 years of DNA. New York: Palgrave Macmillan. p. 95. ISBN 978-1-4039-1479-8.
Coe, Michael D. (2011). The Maya (8th ed.). New York: Thames & Hudson. ISBN 978-0-500-28902-0.
Collins, Roger (1999). Early Medieval Europe: 300–1000 (Second ed.). New York: St. Martin's Press. ISBN 978-0-312-21886-7.
Diamond, Jared (2005). Guns, Germs, and Steel: the Fates of Human Societies. New York: W. W. Norton. ISBN 978-0-393-06131-4.
Ebrey; Walthall; Palais (2006). East Asia: A Cultural, Social, and Political History. Boston, MA: Houghton Mifflin Harcourt Company. ISBN 978-0-618-13384-0.
Fagan, Brian M. (2005). Ancient North America: The Archaeology of a Continent (4th ed.). New York: Thames & Hudson Inc. ISBN 978-0-500-28148-2.
Fasulo, Linda (2015). An Insider's Guide to the UN (3rd ed.). New Haven, CT: Yale University Press. ISBN 978-0-300-20365-3.
Flournoy, Michèle, and Michael Sulmeyer, "Battlefield Internet: A Plan for Securing Cyberspace", Foreign Affairs, vol. 97, no. 5 (September / October 2018), pp. 40–46.
Friedman, Thomas L. (2007). The World is Flat: A Brief History of the Twenty-First Century (Further Updated and Expanded ed.). New York: Picador. ISBN 978-0-312-42507-4.
Gascoigne, Bamber (2003). The Dynasties of China: A History. New York: Carroll & Graf. ISBN 978-1-84119-791-3.
Gernet, Jacques (1996). A History of Chinese Civilization. Cambridge, UK: Cambridge University Press. ISBN 978-0-521-49781-7.
Golden, Peter B. (2011). Central Asia in World History. Oxford, UK: Oxford University Press. ISBN 978-0-19-515947-9.
Grant, Edward (2006). The Foundations of Modern Science in the Middle Ages: Their Religious, Institutional, and Intellectual Contexts. Cambridge, UK: Cambridge University Press. ISBN 978-0-521-56762-6.
Gribbin, John, "Alone in the Milky Way: Why we are probably the only intelligent life in the galaxy", Scientific American, vol. 319, no. 3 (September 2018), pp. 94–99.
Hart-Davis, Adam, ed. (2012). History: The Definitive Visual Guide. New York: DK Publishing. ISBN 978-0-7566-7609-4.
Imber, Colin (2002). The Ottoman Empire, 1300–1650: The Structure of Power. New York: Palgrave Macmillan. ISBN 978-0-333-61386-3.
Johnson, Lonnie R. (1996). Central Europe: Enemies, Neighbors, Friends. New York: Oxford University Press. ISBN 978-0-19-510071-6.
Kelly, Christopher (2007). The Roman Empire: A Very Short Introduction. Oxford, UK: Oxford University Press. ISBN 978-0-19-280391-7.
Kim, Jim Yong, "The Human Capital Gap: Getting Governments to Invest in People", Foreign Affairs, vol. 97, no. 4 (July–August), pp. 92–101.
Kornbluh, Karen, "The Internet's Lost Promise and How America Can Restore It", Foreign Affairs, vol. 97, no. 5 (September / October 2018), pp. 33–38.
Lee, Yun Kuen (2002). "Building the Chronology of Early Chinese History" (PDF). Asian Perspectives. 41 (1): 15–42. doi:10.1353/asi.2002.0006. hdl:10125/17161. ISSN 1535-8283.
Lewis, Mark Edward (2009). China's Cosmopolitan Empire: The Tang Dynasty. Cambridge, MA: Harvard University Press. ISBN 978-0-674-03306-1.
Loyn, H. R. (1991). "Empire, Holy Roman". In Loyn, H. R. (ed.). The Middle Ages: A Concise Encyclopedia. London: Thames and Hudson. pp. 122–23. ISBN 978-0-500-27645-7.
Malley, Robert and Jon Finer, "The Long Shadow of 9/11: How Counterterrorism Warps U.S. Foreign Policy", Foreign Affairs, vol. 97, no. 4 (July–August 2018), pp. 58–69.
Martin, Thomas (2000) [1996]. Ancient Greece: From Prehistoric to Hellenic Times (Revised ed.). New Haven, CT: Yale University Press. ISBN 978-0-300-08493-1.
McCormick, Thomas J. (1995). America's Half-Century: United States Foreign policy in the Cold War and After. Baltimore, MD: Johns Hopkins University Press. ISBN 978-0-8018-5010-3.
Bill McKibben, "A Very Grim Forecast" (partly a review of Global Warming of 1.5 [degree] C: an IPCC Special Report by the Intergovernmental Panel on Climate Change. Available at www.ipcc.ch), The New York Review of Books, vol. LXV, no. 18 (22 November 2018), pp. 4, 6, 8.
McKibben, Bill, "Catastrophic Climate Change", The Nation, vol. 307, no. 2 ( 16/23 July 2018), pp. 18–20.
McKibben, Bill, "Life on a Shrinking Planet: With wildfires, heat waves, and rising sea levels, large tracts of the earth are at risk of becoming uninhabitable", The New Yorker, 26 November 2018, pp. 46–55.
McNeill, William H. (1999) [1967]. A World History (4th ed.). New York: Oxford University Press. ISBN 978-0-19-511616-8.
McNeill, William H. (1982). The Pursuit of Power: Technology, Armed Force, and Society Since A.D. 1000. Chicago, IL: University of Chicago Press. ISBN 978-0-226-56157-8.
Mercer, Samuel Alfred Browne (1949). The religion of ancient Egypt. Luzac. p. 259.
Nilekani, Nandan, "Data to the People: India's Inclusive Internet", Foreign Affairs, vol. 97, no. 5 (September / October 2018), pp. 19–26.
Price, Simon; Thonemann, Peter (2010). The Birth of Classical Europe: A History from Troy to Augustine. New York: Penguin Books.
Roberts, J. M.; Westad, Odd Arne (2013). The Penguin History of the World (Sixth ed.). New York: Penguin Books. ISBN 978-1-84614-443-1.
Shaw, Stanford (1976). History of the Ottoman Empire and Modern Turkey: Volume I: Empire of the Gazis: The Rise and Decline of the Ottoman Empire 1280-1808. Cambridge, UK: Cambridge University Press. ISBN 978-0-521-29163-7.
Sherman, Wendy R., "How We Got the Iran Deal: And Why We'll Miss It", Foreign Affairs, vol. 97, no. 5 (September / October 2018), pp. 186–97.
Singh, Upinder (2008). A History of Ancient and Early Medieval India: From the Stone Age to the 12th Century. Delhi: Pearson Education. ISBN 978-81-317-1120-0.
Stearns, Peter N.; Langer, William L., eds. (2001). The Encyclopedia of World History: Ancient, Medieval, and Modern, Chronologically Arranged (Sixth ed.). Boston: Houghton Mifflin Company. ISBN 978-0-395-65237-4.
Stiglitz, Joseph E., "A Rigged Economy: And what we can do about it" (The Science of Inequality), Scientific American, vol. 319, no. 5 (November 2018), pp. 57–61.
Sullivan, Larry E. (2009). The SAGE glossary of the social and behavioral sciences. Editions SAGE. ISBN 978-1-4129-5143-2.
Ṭabīb, Rashīd al-Dīn; Faḍlallāh, Rašīd-ad-Dīn; Nishapuri, Zahir al-Din; Nīšāpūrī, Ẓahīr-ad-Dīn (2001). Bosworth, Clifford Edmund (ed.). The History of the Seljuq Turks from the Jāmiʻ Al-tawārīkh: An Ilkhanid Adaptation of the Saljūq-nāma of Ẓahīr Al-Dīn Nīshāpūrī. Translated by Luther, Kenneth Allin. Psychology Press. p. 9. ISBN 0-7007-1342-5. [T]he Turks were illiterate and uncultivated when they arrived in Khurasan and depended on Iranian scribes, poets, jurists, and theologians to man the institution of the Empire.
Teeple, John B. (2006). Timelines of World History. New York: DK Publishing. ISBN 978-0-7566-1703-5.
Tudge, Colin (1998). Neanderthals, Bandits and Farmers: How Agriculture Really Began. New Haven, CT: Yale University Press. ISBN 978-0-300-08024-7.
Varghese, Robin, "Marxist World: What Did You Expect From Capitalism?", Foreign Affairs, vol. 97, no. 4 (July–August 2018), pp. 34–42.
Whitfield, Susan (2004). The Silk Road: Trade, Travel, War, and Faith. Serendia Publications, Inc. ISBN 978-1-932476-13-2.
Xue, Zongzheng (1992). A History of Turks. Beijing: Chinese Social Sciences Press.
Further reading
Listen to this article
Spoken Wikipedia icon
This audio file was created from a revision of this article dated 2005-04-19, and does not reflect subsequent edits.
(Audio helpMore spoken articles)
Human history
at Wikipedia's sister projects
Definitions from Wiktionary
Media from Wikimedia Commons
News from Wikinews
Quotations from Wikiquote
Texts from Wikisource
Textbooks from Wikibooks
Resources from Wikiversity
Baten, Joerg, ed. (2016). A History of the Global Economy: 1500 to present. ISBN 978-1-107-50718-0.
Diamond, Jared (1997), Guns, Germs, and Steel, W.W. Norton; updated eds., 2003, 2007.
Fournet, Louis-Henri (1986). Diagrammatic Chart of World History. Editions Sides. ISBN 978-2-86861-096-6.
Frankopan, Peter (2015). The Silk Roads: A New History of the World. Knopf. ISBN 978-1-101-94632-9.
Landes, David (1999). The Wealth and Poverty of Nations. New York: W. W. Norton & Company. ISBN 978-0-393-31888-3.
Landes, David (Spring 2006). "Why Europe and the West? Why Not China?". Journal of Economic Perspectives. 20 (2): 3–22. doi:10.1257/jep.20.2.3.
McNeill, William H. (1963). The Rise of the West: A History of the Human Community. Chicago, IL: University of Chicago Press.
Pomeranz, Kenneth (2000). The Great Divergence: China, Europe and the Making of the Modern World Economy. Princeton.
vte
Social sciences
OutlineHistoryIndex
Primary	
Anthropology (archaeologyculturallinguisticssocial)Economics (microeconomicsmacroeconomicseconometricsmathematical)Geography (humanintegrative)History culturalauxiliary scienceseconomichumanmilitarypoliticalsocial)Law (jurisprudencelegal historylegal systemspublic lawprivate law)Political science (international relationscomparativetheorypublic policy)Psychology (abnormalcognitivedevelopmentalpersonalitysocial)Sociology (criminologydemographyinternetruralurban)
Interdisciplinary	
Administration (businesspublic)AnthrozoologyArea studiesBusiness studiesCognitive scienceCommunication studiesCommunity studiesCultural studiesDevelopment studiesEducationEnvironmental (social sciencestudies)Food studiesGender studiesGlobal studiesHistory of technologyHuman ecologyInformation scienceInternational studiesLinguisticsMedia studiesPhilosophy of science (economicshistorypsychologysocial science)Planning (land useregionalurban)Political ecologyPolitical economyPublic healthRegional scienceScience and technology studiesScience studies historicalQuantum social scienceSocial workVegan studies
List	
List of social science journals
Other categorizations	
HumanitiesGeisteswissenschaftHuman science
Category CategoryCommons page CommonsSocial sciences.svg Society portalWikiversity page Wikiversity
Categories: World historyContemporary history
Navigation menu
Not logged inTalkContributionsCreate accountLog in
ArticleTalk
ReadView sourceView historySearch
Search Wikipedia
Main page
Contents
Current events
Random article
About Wikipedia
Contact us
Donate
Contribute
Help
Community portal
Recent changes
Upload file
Tools
What links here
Related changes
Special pages
Permanent link
Page information
Cite this page
Wikidata item
Print/export
Download as PDF
Printable version
In other projects
Wikimedia Commons
Wikibooks
Languages
Alemannisch
Ænglisc
العربية
Asturianu
Azərbaycanca
বাংলা
Bân-lâm-gú
Беларуская
Беларуская (тарашкевіца)‎
भोजपुरी
བོད་ཡིག
Bosanski
Català
Čeština
Cymraeg
Deutsch
Ελληνικά
Español
Esperanto
Euskara
فارسی
Français
Galego
贛語
गोंयची कोंकणी / Gõychi Konknni
한국어
Հայերեն
हिन्दी
Bahasa Indonesia
Interlingue
Íslenska
Italiano
עברית
Қазақша
Kriyòl gwiyannen
ລາວ
Latina
Latviešu
Lingua Franca Nova
La .lojban.
Magyar
മലയാളം
Bahasa Melayu
Mirandés
မြန်မာဘာသာ
Nederlands
नेपाली
日本語
Norsk bokmål
Norsk nynorsk
Oʻzbekcha/ўзбекча
ਪੰਜਾਬੀ
Patois
Polski
Português
Română
Rumantsch
Русский
Shqip
Simple English
Slovenčina
کوردی
Српски / srpski
Suomi
Svenska
Tagalog
தமிழ்
Татарча/tatarça
తెలుగు
ไทย
Türkçe
Українська
اردو
Tiếng Việt
Winaray
吴语
Xitsonga
ייִדיש
粵語
中文
Edit links
This page was last edited on 11 August 2020, at 13:54 (UTC).
Text is available under the Creative Commons Attribution-ShareAlike License; additional terms may apply. By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization.
Privacy policyAbout WikipediaDisclaimersContact WikipediaDevelopersStatisticsCookie statementMobile viewWikimedia FoundationPowered by MediaWiki